import os
from dotenv import load_dotenv
import requests
import json

load_dotenv()

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY2")
PENGUIN_API_KEY = os.getenv("PENGUIN_API_KEY")
YOUTUBE_API_KEY = os.getenv("YOUTUBE_API_KEY2")

class RequestFailedException(Exception):
    pass


def retrieveAuthorDetails(num_authors=1, best_sellers=True):
    authors_response = requests.get(
        f"https://api.penguinrandomhouse.com/resources/v2/title/domains/PRH.US/authors/views/list-display?rows={num_authors}&showBestsellers={best_sellers}&api_key={PENGUIN_API_KEY}"
    )
    if not authors_response.ok:
        return None
    extended_data = authors_response.json()
    try:
        return extended_data["data"]["authors"]
    except:
        return []


def createBooksFile():
    def retrieveBooksFromAuthors(list_authors):
        # Best selling authors
        final_books = []
        for author_details in list_authors:
            author_details["works"] = []
            try:
                author_id = author_details["authorId"]
                # Get all books written by this author
                author_works_res = requests.get(
                    f"https://api.penguinrandomhouse.com/resources/v2/title/domains/PRH.US/authors/{author_id}/titles?api_key={PENGUIN_API_KEY}"
                )
                if not author_works_res.ok:
                    continue
                author_works = author_works_res.json()["data"]["titles"]
                for work in author_works:
                    work_isbn = work["isbn"]
                    work_title = work["title"]
                    google_res = requests.get(
                        f"https://www.googleapis.com/books/v1/volumes?q={work_title}+isbn:{work_isbn}&key={GOOGLE_API_KEY}"
                    )
                    if not google_res.ok:
                        continue
                    top_result = (
                        google_res.json()["items"][0]
                        if len(google_res.json()["items"])
                        else None
                    )
                    top_result["authorId"] = author_id
                    top_result["isbn"] = work_isbn
                    if top_result:
                        final_books.append(top_result)
                    author_details["works"].append(work_isbn)
            except:
                print("exception")
                continue

        return final_books

    list_authors = retrieveAuthorDetails(200)
    books_list = retrieveBooksFromAuthors(list_authors)

    for author in list_authors[104:]:
        if len(author["works"]):
            filtered_author = {}
            attachAuthorProperties(filtered_author, author)
            author_id = filtered_author["author_id"]
            with open(f"data/authors/{author_id}.json", mode="w") as f:
                json.dump(filtered_author, f)

    for book in books_list:
        filtered_book = {}
        attachBookProperties(filtered_book, book)
        if filtered_book["isbn"] == None:
            continue
        isbn = filtered_book["isbn"]
        with open(f"data/books/{isbn}.json", mode="w") as f:
            json.dump(filtered_book, f)


def attachBookProperties(filtered_book, book):
    try:
        filtered_book["title"] = book["volumeInfo"]["title"]
    except:
        filtered_book["title"] = None
    try:
        filtered_book["subtitle"] = book["volumeInfo"]["subtitle"]
    except:
        filtered_book["subtitle"] = None
    try:
        filtered_book["authors"] = book["volumeInfo"]["authors"]
    except:
        filtered_book["authors"] = []
    try:
        filtered_book["publisher"] = book["volumeInfo"]["publisher"]
    except:
        filtered_book["publisher"] = None
    try:
        filtered_book["published_date"] = book["volumeInfo"]["publishedDate"]
    except:
        filtered_book["published_date"] = None
    try:
        filtered_book["average_rating"] = book["volumeInfo"]["averageRating"]
    except:
        filtered_book["average_rating"] = None
    # try:
    #     search_name = "book " + filtered_book["title"]
    #     google_res = requests.get(
    #         f"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q={search_name}&type=video&key={YOUTUBE_API_KEY}"
    #     )
    #     video_url = google_res.json()["items"][0]["id"]["videoId"]
    #     filtered_book["video_url"] = video_url
    # except:
    #     filtered_book["video_url"] = None
    try:
        filtered_book["description"] = book["volumeInfo"]["description"]
    except:
        filtered_book["description"] = None
    try:
        filtered_book["isbn"] = book["isbn"]
        filtered_book["book_id"] = book["isbn"]
    except:
        filtered_book["isbn"] = None
        filtered_book["book_id"] = None
    try:
        filtered_book["num_ratings"] = book["volumeInfo"]["ratingsCount"]
    except:
        filtered_book["num_ratings"] = None
    try:
        filtered_book["page_count"] = book["volumeInfo"]["pageCount"]
    except:
        filtered_book["page_count"] = None
    try:
        filtered_book["language"] = book["volumeInfo"]["language"]
    except:
        filtered_book["language"] = None
    try:
        filtered_book["price"] = book["saleInfo"]["listPrice"]["amount"]
    except:
        filtered_book["price"] = None
    try:
        filtered_book["currency"] = book["saleInfo"]["listPrice"]["currencyCode"]
    except:
        filtered_book["currency"] = None
    try:
        filtered_book["buy_link"] = book["saleInfo"]["buyLink"]
    except:
        filtered_book["buy_link"] = None
    try:
        filtered_book["small_thumbnail"] = book["volumeInfo"]["imageLinks"][
            "smallThumbnail"
        ]
    except:
        filtered_book["small_thumbnail"] = None
    try:
        filtered_book["regular_thumbnail"] = book["volumeInfo"]["imageLinks"][
            "thumbnail"
        ]
    except:
        filtered_book["regular_thumbnail"] = None
    try:
        filtered_book["author_id"] = book["authorId"]
    except:
        filtered_book["author_id"] = None
    
    filtered_book["library_ids"] = []
    filtered_book["video_url"] = None


def attachAuthorProperties(filtered_author, author):
    try:
        filtered_author["description"] = author["spotlight"]
    except:
        filtered_author["description"] = None
    try:
        filtered_author["name"] = author["display"]
    except:
        filtered_author["name"] = None
    # try:
    #     search_name = "author " + filtered_author["name"]
    #     google_res = requests.get(
    #         f"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q={search_name}&type=video&key={YOUTUBE_API_KEY}"
    #     )
    #     video_url = google_res.json()["items"][0]["id"]["videoId"]
    #     filtered_author["video_url"] = video_url
    # except:
    #     filtered_author["video_url"] = None
    try:
        filtered_author["image"] = author["_links"][0]["href"]
    except:
        filtered_author["image"] = None
    try:
        filtered_author["author_id"] = author["authorId"]
        filtered_author["auth_id"] = author["authorId"]
    except:
        filtered_author["author_id"] = None
        filtered_author["auth_id"] = author["authorId"]
    # try:
    #     filtered_author["image_links"] = author["_links"]
    # except:
    #     filtered_author["image_links"] = []
    try:
        filtered_author["works"] = author["works"]
    except:
        filtered_author["works"] = []
    filtered_author["video_url"] = None


createBooksFile()

# search_name = "author " + " saul bellow"
# google_res = requests.get(f'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q={search_name}&type=video&key={YOUTUBE_API_KEY}')
# video_url = google_res.json()["items"][0]["id"]["videoId"]
# print(video_url)
# filtered_author["video_url"] = video_url
