import unittest
import requests

class FlaskTest(unittest.TestCase):

    # Books
    def test_books_status(self):
        response = requests.get("https://api.bookipedia.me/api/books")
        self.assertEqual(response.status_code, 200)

    def test_books_content(self):
        response = requests.get("https://api.bookipedia.me/api/books")
        self.assertEqual(len(response.json()["books"]), 12)

    def test_books_content_keys(self):
        response = requests.get("https://api.bookipedia.me/api/books")
        response_obj = response.json()
        self.assertEqual(set(response_obj), {"books", "total_pages"})

    def test_book_status_code(self):
        response = requests.get("https://api.bookipedia.me/api/book/9780140067484")
        self.assertEqual(response.status_code, 200)

    def test_book_content_keys(self):
        response = requests.get("https://api.bookipedia.me/api/book/9780140067484")
        response_obj = response.json()
        self.assertEqual(set(response_obj), {"book", "book_author", "related_libraries"})

    # Authors
    def test_authors_status(self):
        response = requests.get("https://api.bookipedia.me/api/authors")
        self.assertEqual(response.status_code, 200)

    def test_authors_content(self):
        response = requests.get("https://api.bookipedia.me/api/authors")
        self.assertEqual(len(response.json()["authors"]), 12)

    def test_authors_content_keys(self):
        response = requests.get("https://api.bookipedia.me/api/authors")
        response_obj = response.json()
        self.assertEqual(set(response_obj), {"authors", "total_pages"})

    def test_author_status_code(self):
        response = requests.get("https://api.bookipedia.me/api/author/14")
        self.assertEqual(response.status_code, 200)

    def test_author_content_keys(self):
        response = requests.get("https://api.bookipedia.me/api/author/14")
        response_obj = response.json()
        self.assertEqual(set(response_obj), {"author", "books_written", "related_libraries"})

    # Libraries
    def test_libraries_status(self):
        response = requests.get("https://api.bookipedia.me/api/libraries")
        self.assertEqual(response.status_code, 200)

    def test_libraries_content(self):
        response = requests.get("https://api.bookipedia.me/api/libraries")
        self.assertEqual(len(response.json()["libraries"]), 12)

    def test_libraries_content_keys(self):
        response = requests.get("https://api.bookipedia.me/api/libraries")
        response_obj = response.json()
        self.assertEqual(set(response_obj), {"libraries", "total_pages"})

    def test_library_status_code(self):
        response = requests.get("https://api.bookipedia.me/api/library/1")
        self.assertEqual(response.status_code, 200)

    def test_library_content_keys(self):
        response = requests.get("https://api.bookipedia.me/api/library/1")
        response_obj = response.json()
        self.assertEqual(set(response_obj), {"library", "library_books", "library_authors"})


if __name__ == "__main__":
    unittest.main()
