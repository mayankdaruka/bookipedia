import json
from flask import Flask, render_template, jsonify, request
from init import app, db
from models import Book, Author, Library, BookSchema, AuthorSchema, LibrarySchema
from sqlalchemy import nullslast, or_

book_schema = BookSchema()
author_schema = AuthorSchema()
library_schema = LibrarySchema()

books_schema = BookSchema(many=True)
authors_schema = AuthorSchema(many=True)
libraries_schema = LibrarySchema(many=True)


@app.route("/")
def hello_world():
    return '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg" alt="cowboy" />'


@app.route("/api/authors", methods=["GET"])
def get_authors():
    queries = request.args.to_dict(flat=False)
    page_size = 12
    page_number = int(queries["page"][0]) if "page" in queries else 1

    filters = []
    sort = None

    rng_query_filters = {'num_works', 'average_book_ratings', 'average_page_counts'}

    names_filters = {'first_name', 'last_name'}

    searches = []

    for query in queries:
        query_value = queries[query][0]
        if query in names_filters:
            [lower_bound, upper_bound] = query_value.split("-")
            sort = nullslast(getattr(Author, query))
            filters.append(getattr(Author, query).between(lower_bound, upper_bound))
        elif query in rng_query_filters:
            [lower_bound, upper_bound] = query_value.split("-")
            filters.append(getattr(Author, query).between(lower_bound, upper_bound))
        elif query == "sort":
            [sort_attr, order] = query_value.split("-")
            sort = nullslast(getattr(Author, sort_attr).desc()) if order == "D" else nullslast(getattr(Author, sort_attr))
        elif query == "search":
            search_terms = [word.lower() for word in query_value.split()]
            for word in search_terms:
                # searches.append(Author.name.match(word))
                searches.append(Author.name.ilike("%{}%".format(word)))
                searches.append(Author.description.ilike("%{}%".format(word)))
                try:
                    searches.append(Author.num_works.in_([int(word)]))
                except ValueError:
                    pass
                try:
                    searches.append(Author.average_book_ratings.in_([int(word)]))
                except ValueError:
                    pass
                try:
                    searches.append(Author.average_page_counts.in_([int(word)]))
                except ValueError:
                    pass
            
    # print(searches)
    all_authors = Author.query.filter(*filters).filter(or_(*tuple(searches))).order_by(sort).paginate(page_number, page_size, False)
    all_author_items = all_authors.items
    # all_authors = Author.query.all()
    authors_list = authors_schema.dump(all_author_items)

    return jsonify({"authors": authors_list, "total_pages": all_authors.pages})


@app.route("/api/books", methods=["GET"])
def get_books():
    queries = request.args.to_dict(flat=False)
    
    page_size = 12
    page_number = int(queries["page"][0]) if "page" in queries else 1

    filters = []
    sort = None

    rng_query_filters = {'published_year', 'average_rating', 'price', 'num_ratings', 'page_count'}

    match_filters = {'language', 'currency'}

    name_filters = {'author_name'}

    searches = []

    for query in queries:
        query_value = queries[query][0]
        if query in match_filters:
            filters.append(getattr(Book, query) == query_value)
        elif query in rng_query_filters:
            [lower_bound, upper_bound] = query_value.split("-")
            filters.append(getattr(Book, query).between(lower_bound, upper_bound))
        elif query in name_filters:
            filters.append(Book.author_name.ilike("%{}%".format(query_value)))
        elif query == "sort":
            [sort_attr, order] = query_value.split("-")
            sort = nullslast(getattr(Book, sort_attr).desc()) if order == "D" else nullslast(getattr(Book, sort_attr))
        elif query == "search":
            search_terms = [word.lower() for word in query_value.split()]
            for word in search_terms:
                # searches.append(Author.name.match(word))
                searches.append(Book.title.ilike("%{}%".format(word)))
                searches.append(Book.subtitle.ilike("%{}%".format(word)))
                searches.append(Book.publisher.ilike("%{}%".format(word)))
                searches.append(Book.author_name.ilike("%{}%".format(word)))
                searches.append(Book.language.ilike("%{}%".format(word)))

                try:
                    searches.append(Book.page_count.in_([int(word)]))
                except ValueError:
                    pass
                try:
                    searches.append(Book.price.in_([int(word)]))
                except ValueError:
                    pass
                try:
                    searches.append(Book.title.in_([int(word)]))
                except ValueError:
                    pass
                try:
                    searches.append(Book.average_rating.in_([int(word)]))
                except ValueError:
                    pass


    all_books = Book.query.filter(*filters).filter(or_(*tuple(searches))).order_by(sort).paginate(page_number, page_size, False)
    all_book_items = all_books.items
    # all_books = Book.query.all()
    books_list = books_schema.dump(all_book_items)


    return jsonify({"books": books_list, "total_pages": all_books.pages})


@app.route("/api/libraries", methods=["GET"])
def get_libraries():
    queries = request.args.to_dict(flat=False)
    
    page_size = 12
    page_number = int(queries["page"][0]) if "page" in queries else 1

    filters = []
    sort = None

    rng_query_filters = {'ann_circ', 'col_size', 'ser_pop', 'lng', 'lat'}

    match_filters = {'county', 'region'}
    
    searches = []

    for query in queries:
        query_value = queries[query][0]
        if query in match_filters:
            filters.append(getattr(Library, query) == query_value)
        elif query in rng_query_filters:
            [lower_bound, upper_bound] = query_value.split("-")
            filters.append(getattr(Library, query).between(lower_bound, upper_bound))
        elif query == "sort":
            [sort_attr, order] = query_value.split("-")
            sort = nullslast(getattr(Library, sort_attr).desc()) if order == "D" else nullslast(getattr(Library, sort_attr))
        elif query == "search":
            search_terms = [word.lower() for word in query_value.split()]
            for word in search_terms:
                # searches.append(Author.name.match(word))
                searches.append(Library.county.ilike("%{}%".format(word)))
                searches.append(Library.region.ilike("%{}%".format(word)))
                searches.append(Library.libname.ilike("%{}%".format(word)))
                searches.append(Library.adname.ilike("%{}%".format(word)))
                searches.append(Library.address.ilike("%{}%".format(word)))

                try:
                    searches.append(Library.ser_pop.in_([int(word)]))
                except ValueError:
                    pass
                try:
                    searches.append(Library.col_size.in_([int(word)]))
                except ValueError:
                    pass
                try:
                    searches.append(Library.ann_circ.in_([int(word)]))
                except ValueError:
                    pass

    all_libraries = Library.query.filter(*filters).filter(or_(*tuple(searches))).order_by(sort).paginate(page_number, page_size, False)
    all_library_items = all_libraries.items
    # all_libraries = Library.query.all()
    libraries_list = libraries_schema.dump(all_library_items)

    return jsonify({"libraries": libraries_list, "total_pages": all_libraries.pages})


@app.route("/api/all_libraries", methods=["GET"])
def get_all_libraries():
    all_libraries = Library.query.all()
    libraries_list = libraries_schema.dump(all_libraries)
    return jsonify({"libraries": libraries_list})

@app.route("/api/all_books", methods=["GET"])
def get_all_books():
    all_books = Book.query.all()
    books_list = books_schema.dump(all_books)
    return jsonify({"books": books_list})

@app.route("/api/all_authors", methods=["GET"])
def get_all_authors():
    all_authors = Author.query.all()
    authors_list = authors_schema.dump(all_authors)
    return jsonify({"authors": authors_list})


@app.route("/api/book/<isbn>", methods=["GET"])
def get_book(isbn):
    book = Book.query.get(isbn)
    book_obj = book_schema.dump(book)
    author_id = book_obj["author_id"]
    related_author = Author.query.get(author_id)
    related_libraries = []

    if book_obj["library_ids"]:
        for library_id in book_obj["library_ids"]:
            related_library = Library.query.get(library_id)
            related_library_obj = library_schema.dump(related_library)
            related_libraries.append(related_library_obj)

    return jsonify(
        { "book": book_obj, "book_author": author_schema.dump(related_author), "related_libraries": related_libraries }
    )


@app.route("/api/author/<id>", methods=["GET"])
def get_author(id):
    author = Author.query.get(id)
    author_obj = author_schema.dump(author)
    author_works = author_obj["works"]
    list_books = []

    for isbn in author_works:
        book = Book.query.get(isbn)
        book_obj = book_schema.dump(book)
        list_books.append(book_obj)

    # related_library_obj = {}
    related_libraries = []
    if "library_ids" in author_obj:
        for library_id in author_obj["library_ids"]:
            related_library = Library.query.get(library_id)
            # related_library_obj = library_schema.dump(related_library)
            related_libraries.append(related_library)

    return jsonify({"author": author_schema.dump(author), "books_written": list_books, "related_libraries": libraries_schema.dump(related_libraries) })


@app.route("/api/library/<id>", methods=["GET"])
def get_library(id):
    library = Library.query.get(id)
    library_obj = library_schema.dump(library)

    related_books = []
    related_authors = []

    if "books" in library_obj and len(library_obj["books"]):
        for book_id in library_obj["books"]:
            book = Book.query.get(book_id)
            book_obj = book_schema.dump(book)
            related_books.append(book_obj)

    if "authors" in library_obj and len(library_obj["authors"]) > 1:
        for author_id in library_obj["authors"][1:]:
            author = Author.query.get(author_id)
            author_obj = author_schema.dump(author)
            related_authors.append(author_obj)

    return jsonify({"library": library_obj, "library_books": related_books, "library_authors": related_authors })


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
