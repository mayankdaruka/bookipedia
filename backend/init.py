from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import os
# from sqlalchemy import create_engine, Column, String, Integer

load_dotenv()

DATABASE_URI = os.getenv("AWS_DB_KEY")

app = Flask(__name__)
CORS(app)

app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URI

db = SQLAlchemy(app)
