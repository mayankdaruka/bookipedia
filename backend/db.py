from init import db
from models import Book, Author, Library
import os
import json
from dotenv import load_dotenv
import requests

load_dotenv()

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY2")
PENGUIN_API_KEY = os.getenv("PENGUIN_API_KEY")
YOUTUBE_API_KEY = os.getenv("YOUTUBE_API_KEY2")

def reset_db():
    # Resets the database
    db.session.remove()
    db.drop_all()
    db.create_all()
    print("Database reset")


def populate_authors():
    dir_name = "data/authors"
    mapping_name = "data/authors_to_libraries"
    books_dir_name = "data/books"

    for filename in os.listdir(dir_name):
        path_name = dir_name + "/" + filename
        with open(path_name, "r") as f:
            author = json.load(f)
            author_id = author["author_id"]
            author_works = author["works"]
            try:
                with open(f"{mapping_name}/{author_id}.json", "r") as lib_file:
                    mapping = json.load(lib_file)
                    lib_ids = mapping["lib_ids"]
                    author["library_ids"] = lib_ids
            except:
                author["library_ids"] = []
            
            author["num_works"] = len(author_works)
            total_page_count = 0
            books_with_page_counts = 0
            total_ratings = 0
            books_with_ratings = 0
            for work_id in author_works:
                try:
                    with open(f"{books_dir_name}/{work_id}.json", "r") as book_file:
                        book = json.load(book_file)
                        if book["page_count"]:
                            total_page_count += book["page_count"]
                            books_with_page_counts += 1
                        if book["average_rating"]:
                            total_ratings += book["average_rating"]
                            books_with_ratings += 1
                except:
                    print("exception")
            try:
                with open(f"data/authorVideos.json", "r") as video_file:
                    video_mapping = json.load(video_file)
                    author_name_lowercase = author["name"].lower()
                    if author_name_lowercase in video_mapping:
                        author["video_url"] = video_mapping[author_name_lowercase]
                    else:
                        author["video_url"] = None
            except:
                print()

            author_name_split = author["name"].split()
            author["first_name"] = author_name_split[0]
            author["last_name"] = author_name_split[-1]
            
            author["average_book_ratings"] = total_ratings/books_with_ratings if books_with_ratings > 0 else 0
            author["average_page_counts"] = total_page_count//books_with_page_counts if books_with_page_counts > 0 else 0
            author_db_instance = Author(**author)
            db.session.add(author_db_instance)
    db.session.commit()


def populate_books():
    dir_name = "data/books"
    author_dir_name = "data/authors"
    mapping_name = "data/books_to_libraries"
    for filename in os.listdir(dir_name):
        path_name = dir_name + "/" + filename
        with open(path_name, "r") as f:
            book = json.load(f)
            if "book_id" not in book or book["book_id"] == None:
                continue
            if "library_id" in book:
                del book["library_id"]

            book_id = book["book_id"]
            
            try:
                with open(f"{mapping_name}/{book_id}.json", "r") as lib_file:
                    mapping = json.load(lib_file)
                    lib_ids = mapping["lib_ids"]
                    book["library_ids"] = lib_ids
            except:
                book["library_ids"] = []

            with open(f"data/bookVideos.json", "r") as video_file:
                video_mapping = json.load(video_file)
                book_name = book["title"]
                if book_name in video_mapping:
                    book["video_url"] = video_mapping[book_name]
                else:
                    book["video_url"] = None

            author_id = book["author_id"]
            with open(f"{author_dir_name}/{author_id}.json", "r") as author_file:
                author_obj = json.load(author_file)
                author_name = author_obj["name"]
                book["author_name"] = author_name
                book["author_first_name"] = author_name.split()[0]
                book["author_last_name"] = author_name.split()[-1]

            try:
                book["published_year"] = int(book["published_date"][:4])
            except:
                print("no published year")
                book["published_year"] = None

            book_db_instance = Book(**book)
            
            db.session.add(book_db_instance)
    db.session.commit()


def populate_libraries():
    dir_name = "data/libraries"
    regions_list = {}
    county_list = {}
    for filename in os.listdir(dir_name):
        path_name = dir_name + "/" + filename
        with open(path_name, "r") as f:
            library = json.load(f)
            ann_circ = library["ann_circ"]
            col_size = library["col_size"]
            ser_pop = library["ser_pop"]
            regions_list[library["region"]] = True
            county_list[library["county"]] = True
            try: 
                library["ann_circ"] = int(ann_circ.replace(",", ""))
                library["col_size"] = int(col_size.replace(",", ""))
                library["ser_pop"] = int(ser_pop.replace(",", ""))
                library["county"] = library["county"].strip()

                library_db_instance = Library(**library)
                db.session.add(library_db_instance)
            except:
                print(ann_circ)
                print(col_size)
                print(ser_pop)

    db.session.commit()
    # print(regions_list)
    # print(county_list)


if __name__ == "__main__":
    reset_db()
    populate_authors()
    populate_books()
    populate_libraries()
