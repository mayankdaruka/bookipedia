from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import ARRAY
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field

from init import db

link_books_authors = db.Table(
    "link_books_authors",
    db.Column("book_isbn", db.String(), db.ForeignKey("book.isbn"), primary_key=True),
    db.Column(
        "author_id", db.Integer, db.ForeignKey("author.author_id"), primary_key=True
    ),
)

# link_books_libraries = db.Table(
#     "link_books_libraries",
#     db.Column("book_isbn", db.String(), db.ForeignKey("book.isbn"), primary_key = True),
#     db.Column("library_id", db.Integer, db.ForeignKey("library.id"), primary_key = True)
# )


class Book(db.Model):
    __tablename__ = "book"
    author_id = db.Column(db.String())
    book_id = db.Column(db.String())
    library_ids = db.Column(ARRAY(db.Integer))

    authors = db.relationship(
        "Author",
        secondary=link_books_authors,
        # backref=db.backref("books", lazy="dynamic"),
    )

    # Attributes to show on model component
    title = db.Column(db.String())
    subtitle = db.Column(db.String())
    authors = db.Column(ARRAY(db.String()))
    price = db.Column(db.Float)
    currency = db.Column(db.String())

    # Attributes to show on instance page component
    publisher = db.Column(db.String())
    published_date = db.Column(
        db.String()
    )  # Maybe convert to unix timestamp and store that
    published_year = db.Column(db.Integer)
    isbn = db.Column(db.String(), db.ForeignKey("book.isbn"), primary_key=True)
    page_count = db.Column(db.Integer)
    video_url = db.Column(db.String())
    # height = db.Column(db.String()) # will include units (ex. cm)
    # width = db.Column(db.String()) # will include units
    average_rating = db.Column(db.Float)
    num_ratings = db.Column(db.Integer)
    language = db.Column(db.String())
    description = db.Column(db.String())
    buy_link = db.Column(db.String())
    author_name = db.Column(db.String())
    author_first_name = db.Column(db.String())
    author_last_name = db.Column(db.String())

    # genre = db.Column(db.String())

    # Media
    small_thumbnail = db.Column(db.String())
    regular_thumbnail = db.Column(db.String())
    # medium_image = db.Column(db.String())
    # large_image = db.Column(db.String())


class Author(db.Model):
    __tablename__ = "author"
    author_id = db.Column(db.Integer, primary_key=True)
    auth_id = db.Column(db.Integer)
    library_ids = db.Column(ARRAY(db.Integer))

    # books = db.relationship(
    #     "Book",
    #     secondary=link_books_authors,
    #     # backref=db.backref("authors", lazy="dynamic"),
    # )

    # Attributes to show on model component
    name = db.Column(db.String())
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    video_url = db.Column(db.String())

    num_works = db.Column(db.Integer)
    average_book_ratings = db.Column(db.Float)
    average_page_counts = db.Column(db.Integer)

    # Attributes to show on instance page component
    description = db.Column(db.String())
    works = db.Column(ARRAY(db.String()))

    # Media
    image = db.Column(db.String())
    # media1
    # media2


class Library(db.Model):
    __tablename__ = "library"
    id = db.Column(db.Integer, primary_key=True)
    # library_id = db.Column(db.Integer, primary_key = True)

    # Attributes to show on model component
    libname = db.Column(db.String())
    adname = db.Column(db.String())
    address = db.Column(db.String())
    liburl = db.Column(db.String())
    phone = db.Column(db.String())
    county = db.Column(db.String())
    region = db.Column(db.String())
    ser_pop = db.Column(db.Integer)
    col_size = db.Column(db.Integer)
    ann_circ = db.Column(db.Integer)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    img_link = db.Column(db.String())
    books = db.Column(ARRAY(db.String()))
    authors = db.Column(ARRAY(db.Integer))

    # Attributes to show on instance page component

    # Media


class BookSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Book
        include_relationships = True
        load_instance = True


# Author Schema
class AuthorSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Author
        include_relationships = True
        load_instance = True


# Library Schema
class LibrarySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Library
        include_relationships = True
        load_instance = True
