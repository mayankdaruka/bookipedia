# from init import db
import os
import json
from dotenv import load_dotenv
import requests

load_dotenv()

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY2")
PENGUIN_API_KEY = os.getenv("PENGUIN_API_KEY")
YOUTUBE_API_KEY = os.getenv("YOUTUBE_API_KEY3")

def populate_books():
    dir_name = "data/books"

    for filename in os.listdir(dir_name)[3:100]:
        path_name = dir_name + "/" + filename
        with open(path_name, "r") as f:
            book = json.load(f)
            if "book_id" in book:
                book_id = book["book_id"]
                print(book_id)

                try:
                    search_name = "book " + book["title"]
                    google_res = requests.get(f"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q={search_name}&type=video&key={YOUTUBE_API_KEY}")
                    # print(google_res.json())
                    video_url = google_res.json()["items"][0]["id"]["videoId"]
                    book["video_url"] = video_url
                except:
                    print("exception")
            
                with open(path_name, mode="w") as newfile:
                    json.dump(book, newfile)
            
populate_books()
            