# bookipedia

| Name          | UTEID   | GitLab ID     |
| ------------- | ------- | ------------- |
| Mayank Daruka | md43627 | mayankdaruka  |
| William Shi   | sm68866 | shiguy        |
| Megan Noronha | mnn553  | megan.noronha |
| Jin Huang     | jh72989 | huangjin88    |
| John Wang     | zw5797  | zhongyi.wang  |

## Git SHA

- Phase 1: 390e44d60a4948ac88e125bcd2f9063e4e720db2
- Phase 2: 3cf1519b359a02b02c9b2def6a1f3fb166cb74ba
- Phase 2 (Resubmission): b625cbc57d01f72fbc128f526348cc1c8a99a037
- Phase 3: 43821055752cad95d51dc1af14622a0e6ce161b0
- Phase 4: da24c49616e99ed42e255a453d07af78238b57aa

## Project Leader

- Phase 1: Mayank Daruka
- Phase 2: Megan Noronha
- Phase 3: Jin Huang
- Phase 4: John Wang

Gitlab Pipeline: https://gitlab.com/mayankdaruka/bookipedia/-/pipelines

Website Link: https://www.bookipedia.me/

| Name          | Estimated (Phase 1) | Actual (Phase 1) | Estimated (Phase 2) | Actual (Phase 2) |
| ------------- | ------------------- | ---------------- | ------------------- | ---------------- |
| Mayank Daruka | 10                  | 24               | 25                  | 45               |
| William Shi   | 10                  | 20               | 15                  | 30               |
| Megan Noronha | 25                  | 25               | 25                  | 35               |
| Jin Huang     | 15                  | 25               | 25                  | 35               |
| John Wang     | 12                  | 24               | 18                  | 30               |

| Name          | Estimated (Phase 3) | Actual (Phase 3) | Estimated (Phase 4) | Actual (Phase 4) |
| ------------- | ------------------- | ---------------- | ------------------- | ---------------- |
| Mayank Daruka | 15                  | 20               | 5                   | 3                |
| William Shi   | 10                  | 15               | 5                   | 1                |
| Megan Noronha | 15                  | 25               | 10                  | 10               |
| Jin Huang     | 15                  | 25               | 10                  | 10               |
| John Wang     | 10                  | 15               | 5                   | 2                |

Comments: None
