.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

format:
	black ./*.py

install:
	pip install -r ./backend/requirements.txt

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        

# check the existence of check files
check: $(CFILES)

python-unit-tests:
	python3 backend/tests.py -v

format:
	black ./backend/*.py

build-image:	
	docker build -t selenium-image -f Dockerfile .

selenium-image:
	docker run --rm -i -t \
	 -v $(pwd):/usr/src \
	-p 5000:5000 \
	 -w /usr/src \
	 selenium-image

selenium-tests:
	chmod 775 frontend/chromedriver.exe
	chmod 775 frontend/chromedriver_linux
	python3 frontend/test.py

