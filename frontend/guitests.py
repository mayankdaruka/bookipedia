import unittest
import time
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select


PATH = "./frontend/chromedriver.exe"
URL = "https://www.bookipedia.me"

class Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        cls.driver = webdriver.Chrome(PATH, options=chrome_options)
        cls.driver.get(URL)
        cls.actions = ActionChains(cls.driver)
        cls.root_url = "https://www.bookipedia.me/"
        # cls.root_url = "http://localhost:3000/"

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    # Click home button on nav bar
    def test1(self):
        self.driver.find_element_by_class_name("navigation").click()
        self.driver.find_element_by_xpath('//*[@id="root"]/div/div/nav/div/div/ul/li[1]/a').click()
        currentURL = self.driver.current_url
        print("test 1 " + currentURL)
        print("test 1 " + self.root_url)
        assert "https://www.bookipedia.me/" == currentURL

    # Click about button on nav bar
    def test2(self):
        self.driver.find_element_by_class_name("navigation").click()
        self.driver.find_element_by_xpath('//*[@id="root"]/div/div/nav/div/div/ul/li[2]/a').click()
        currentURL = self.driver.current_url
        newurl = self.root_url + "about"
        assert  newurl == currentURL

    # Click books button on nav bar
    def test3(self):
        self.driver.find_element_by_class_name("navigation").click()
        self.driver.find_element_by_xpath('//*[@id="root"]/div/div/nav/div/div/ul/li[3]/a').click()
        currentURL = self.driver.current_url
        newurl = self.root_url+ "books"
        print("test 2 current " + currentURL)
        print("test 2 " + self.root_url)
        assert  newurl == currentURL

    # Click authors button on nav bar
    def test4(self):
        self.driver.find_element_by_class_name("navigation").click()
        self.driver.find_element_by_xpath('//*[@id="root"]/div/div/nav/div/div/ul/li[4]/a').click()
        currentURL = self.driver.current_url
        newurl = self.root_url + "authors"
        assert  newurl == currentURL

    def test5(self):
        self.driver.find_element_by_class_name("navigation").click()
        self.driver.find_element_by_xpath('//*[@id="root"]/div/div/nav/div/div/ul/li[5]/a').click()
        currentURL = self.driver.current_url
        newurl = self.root_url+ "libs"
        assert newurl == currentURL

    def test6(self):
        self.driver.get(f"{self.root_url}authors")
        # test clicking on item
        element = self.driver.find_element_by_tag_name('h1')
        result = element.text 
        assert result == "Authors"

    def test7(self):
        self.driver.get(f"{self.root_url}books")
        # test clicking on item
        element = self.driver.find_element_by_tag_name('h1')
        result = element.text 
        assert result == "Books"

    # def test8(self):
    #     self.driver.get(f"{self.root_url}")
    #     # test clicking on item
    #     element = self.driver.find_element_by_tag_name('h1')
    #     result = element.text 
    #     assert result == "Introduction"

    def test9(self):
        self.driver.get(f"{self.root_url}authors")
        # test clicking on item
        element = self.driver.find_element_by_tag_name('h1')
        result = element.text 
        assert result == "Authors"

    def test10(self):
        self.driver.get(f"{self.root_url}authors")
        # test clicking on item
        element = self.driver.find_element_by_tag_name('p')
        result = element.text 
        assert result == "Authors use their amazing talents to create magical worlds. Explore this page to find out more about the people behind the books."


    ## Test Book Filter ###
    def test11(self):
        newurl = self.root_url + "books"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[0]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "books?author_name=Andrew%20Carroll&page=1"
        currentURL = self.driver.current_url
        assert newurl == currentURL

    def test12(self):
        newurl = self.root_url + "books"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[1]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "books?page=1&price=0-20"
        currentURL = self.driver.current_url
        assert newurl == currentURL

    def test13(self):
        newurl = self.root_url + "books"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[2]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "books?page=1&page_count=100-500"
        currentURL = self.driver.current_url
        assert newurl == currentURL

    def test14(self):
        newurl = self.root_url + "books"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[3]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "books?average_rating=0-2&page=1"
        currentURL = self.driver.current_url
        assert newurl == currentURL


    def test15(self):
        newurl = self.root_url + "books"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[4]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "books?page=1&published_year=1940-1950"
        currentURL = self.driver.current_url
        print("test 1 " + currentURL)
        print("test 1 " + newurl)
        assert newurl == currentURL


    ## Test Author Search ###
    def test16(self):
        newurl = self.root_url + "authors"
        self.driver.get(newurl)
        self.driver.find_element_by_id("user-button-click").click()
        newurl = self.root_url + "search/q=/model=authors"
        currentURL = self.driver.current_url
        assert newurl == currentURL

        
    ### Test Library Sort and filter ###
    def test17s(self):
        newurl = self.root_url + "libs"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[5]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "libs?page=1&sort_libs=county-A"
        currentURL = self.driver.current_url
        assert newurl == currentURL

    def test18s(self):
        newurl = self.root_url + "libs"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[0]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "libs?page=1&region=West%20Texas"
        currentURL = self.driver.current_url
        assert newurl == currentURL


    ### Test Authors Sort and filter ###

    def test19s(self):
        newurl = self.root_url + "authors"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[0]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "authors?first_name=G-M&page=1"
        currentURL = self.driver.current_url
        assert newurl == currentURL


    def test20s(self):
        newurl = self.root_url + "authors"
        self.driver.get(newurl)
        element = WebDriverWait(self.driver, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, "MuiSelect-root")))
        selections = self.driver.find_element_by_class_name('row')
        col = selections.find_elements_by_class_name('col')[1]
        temp = col.find_element_by_class_name('MuiSelect-root')
        temp.send_keys('\n')
        self.actions.send_keys(Keys.DOWN, Keys.DOWN, Keys.RETURN).perform()
        newurl = self.root_url + "authors?last_name=B-C&page=1"
        currentURL = self.driver.current_url
        assert newurl == currentURL


   


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])

