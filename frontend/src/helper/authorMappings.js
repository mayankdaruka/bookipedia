const first_name_mapping_authors = {
	"None": "None", 
    "A-F": "A - F",
    "G-M": "G - M",
    "N-T": "N - T",
    "U-Z": "U - Z",
}

const last_name_mapping_authors = {
	"None": "None", 
    "A-B": " A ",
    "B-C": " B ",
    "C-D": " C ",
    }
    

const avg_ratings_mapping_authors = {
	"None": "None", 
	"0-1": "<= 1 star",
	"0-2": "<= 2 star",
	"0-3": "<= 3 star",
	"0-4": "<= 4 star",
	"0-5": "<= 5 star",
}

// min is 12, max is 4528
const avg_pc_mapping_authors = {
	"None": "None", 
	"0-100": "0 - 100",
	"100-300": "100 - 300",
	"300-500": "300 - 500",
	"500-750": "500 - 750",
	"750-1000": "750- 1000",
	"1000-1500": "1000 - 1500",
}

// num works min -1, max = 10
const num_works_mapping_authors = {
	"None": "None", 
	"0-1": "<= 1 books",
	"0-3": "<= 3 books",
	"0-5": "<= 5 books",
	"0-7": "<= 7 books",
	"0-10": "<= 10 books",
}


//  rng_query_filters = {'num_works', 'average_book_ratings', 'average_page_counts'} (what it is called on backend)
const sort_mapping_authors = {
	"None": "None", 
	"first_name-A": "Author First Name (A-Z)",
	"first_name-D": "Author First Name (Z-A)",
    "last_name-A": "Author Last Name (A-Z)",
	"last_name-D": "Author Last Name (Z-A)",
	"average_book_ratings-A": "Avg. Books Ratings(Asc)",
	"average_book_ratings-D": "Avg. Books Ratings (Desc)",
	"average_page_counts-A": "Avg. Book Length (Asc)",
	"average_page_counts-D": "Avg. Book Length (Desc)",
	"num_works-A": "Num Works (Asc)",
	"num_works-D": "Num Works (Desc)",
}



// Library data
// col  min - 7649, max - 4131672
// annula min- 1303, max- 10236949
// serv min - 844, max - 2145146

// Author Data
// num works min -1, max = 10
export {
	first_name_mapping_authors,
	last_name_mapping_authors,
	avg_ratings_mapping_authors,
	avg_pc_mapping_authors,
	num_works_mapping_authors,
	sort_mapping_authors,
}
