import { languageData } from "./constants"


export const enumerate = (arr) => (arr ? arr.map(e => <>{`${e} `}</>) : "")

export const splitNames = (arr) => (arr ? arr.split(" ", 2) : "")


// AUTHOR FUNCTIONS
// Functions to calculate author values, most on backend now, not needed
// To get the average ratings of all the books of the authors (on backend now)


export const get_avg_ratings = (books) => {
  let sum = 0
  let total = books.length

  for (let i = 0; i < books.length; i++) {
    sum = books[i]['average_rating'] === null ? sum + 0 : sum + books[i]['average_rating']
  }
  return Math.round((sum / total) * 100) / 100;
}

// To get the average page counts of all the books of the authors (on backend now)
export const get_page_count = (books) => {
  // return an array of
  let sum = 0
  let total = books.length

  for (let i = 0; i < books.length; i++) {
    sum = books[i]['page_count'] === null ? sum + 0 : sum + books[i]['page_count']
  }

  return Math.trunc(sum / total);
}


// To get some publisher of the authors books (still used)
export const get_publisher = (books) => {

  let publisher = ""
  for (let i = 0; i < books.length; i++) {
    publisher = books[i]['publisher'] === null ? publisher : books[i]['publisher']

  }

  return publisher;
}

// To get  some language out of the authors books(still used)
export const get_lang = (books) => {
  let language = ""
  for (let i = 0; i < books.length; i++) {

    language = books[i]['language'] === null ? language : get_proper_name(books[i]['language'])
  }
  return language;
}


// Veery janky way to get the gender of the author, pretty flawed, if time, change
export const get_gender = (description) => {
  let gender = ""
  let descr =  description + ""


  if (descr.includes("He") || descr.includes("he")) {
    gender = "Male"
  }

  if (descr.includes("She") || descr.includes("she")) {
    gender = "Female"
  } else {
    gender = "Male"
  }
  return gender;
}

// Gets the latest book the author has written by checking the publishing dates of all the books
export const get_latest_book = (books) => {

  let book_title = ""
  let max = 0
  let date = 0

  for (let i = 0; i < books.length; i++) {

    date = books[i]['published_date'] === null ? 0 : books[i]['published_date']
    if(date > max){
      max = date
      book_title = books[i]['title']
    }
 
  }

  return book_title;
}

// BOOK FUNCTIONS
// Get the language name given the encoding (ie, looks up "EN" returns English), Used in Books

export const get_proper_name = (code) => {
  let proper_name = ""

  for (let i = 0; i < languageData.length; i++) {
    if (languageData[i]['code'] === code) {
      proper_name = languageData[i]['name']
    }
  }
  return proper_name;

}


//FILTER FUNCTIONS
// To update the filter and sort params in the url for each model
export const updateParams  = (name, value, setParams, params) =>{     
  setParams({
      ...params,
      [name]: value, // takes in key
      page: 1,
  })
}

// to update the page values in the url for each model
export const updateParamsPageNum = (page, setParams, params) => {
  // console.log("value",page )
  setParams({
    ...params,
    page: page,
    //[sort] sort=price ()
})

}
