import {
    name_mapping_books,
    price_mapping_books,
	page_count_mapping_books,
    rating_mapping_books,
    year_mapping_books,	
	sort_mapping_books,
} from "./bookMappings"

import{
    first_name_mapping_authors,
	last_name_mapping_authors,
	avg_ratings_mapping_authors,
	avg_pc_mapping_authors,
	num_works_mapping_authors,
	sort_mapping_authors,
}from "./authorMappings"


import{
    region_mapping_libs,
    county_mapping_libs,
    ser_pop_mapping_libs,
    col_size_mapping_libs,
    ann_circ_mapping_libs,
    sort_mapping_libs,
} from "./libMappings"
// import { identityObjectFromArray } from "library/Functions"

// "region-A": "Region (A-Z)",
// "region-D": "Region (Z-A)",
// "county-A": "County (A-Z)",
// "county-D": "County (Z-A)",
// "ser_pop-A": "Service Population (Asc)",
// "ser_pop-D": "Service Population  (Desc)",
// "col_size-A": "Collection Size (Asc)",
// "col_size-D": "Collection Size (Desc)",
// "ann_circ-A": "Annual Circulation (Asc)",
// "ann_circ-D": "Annual Circulation (Desc)",

// region: "Region",
// county: "County",
// ser_pop: "Service Population",
// col_size: "Collection Size",
// ann_circ: "Annual Circulation",
// sort_authors: "Sort Libraries",

const filterOptionsMap = {
    //Books
    "Author Name": name_mapping_books,
    "Price": price_mapping_books,
    "Page Count": page_count_mapping_books,
    "Ratings": rating_mapping_books,
	"Publish Year": year_mapping_books,
    "Sort Books":sort_mapping_books,

    //Authors
    "First Name": first_name_mapping_authors,
    "Last Name": last_name_mapping_authors,
    "Avg. Books Ratings": avg_ratings_mapping_authors,
    "Avg. Book Length": avg_pc_mapping_authors,
	"Num Works": num_works_mapping_authors,
    "Sort Authors":sort_mapping_authors,

    // Libraries
    "Region": region_mapping_libs,
    "County": county_mapping_libs,
    "Service Population": ser_pop_mapping_libs,
    "Collection Size": col_size_mapping_libs,
	"Annual Circulation": ann_circ_mapping_libs,
    "Sort Libraries":sort_mapping_libs,
}

//rng_query_filters = {'year', 'average_rating', 'price', 'num_ratings', 'page_count'}
// We will do average ratings as a sort
//num_works', 'average_book_ratings', 'average_page_counts

const filterTitlesMap = {
    //Books param names
    author_name: "Author Name", //change to what mayank puts!!
	price: "Price",
    page_count: "Page Count",
    average_rating: "Ratings",
    published_year: "Publish Year",
    sort_books: "Sort Books",

    //Author param names
    first_name: "First Name",
    last_name: "Last Name",
    average_book_ratings: "Avg. Books Ratings",
    average_page_counts: "Avg. Book Length",
    num_works: "Num Works",
    sort_authors: "Sort Authors",


    // Lib param naames:
    region: "Region",
    county: "County",
    ser_pop: "Service Population",
    col_size: "Collection Size",
    ann_circ: "Annual Circulation",
    sort_libs: "Sort Libraries",
}

export { filterOptionsMap, filterTitlesMap }
