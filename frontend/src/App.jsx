import React from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  Navigation,
  Home,
  About,
  Books,
  Authors,
  Posts,
  Libs,
  Results
} from "./components";
import SingleBook from "./pages/SingleBook/SingleBook";
import SingleAuthor from "./pages/SingleAuthor/singleAuthor";
import SingleLib from "./pages/SingleLib/SingleLib";

import OurData from "./pages/OurData/OurData";
import ProviderData from "./pages/ProviderData/ProviderData";


import {QueryParamProvider} from "use-query-params"

function App() {

  return (
    <div className="App" style={{ backgroundColor: "#fcfcfc", minHeight: "100vh"}} >
      {/* style={{ backgroundColor: "#fcfcfc", minHeight: "100vh"}} */}
      <Router>
      <QueryParamProvider ReactRouterRoute={Route}>
        <Navigation />
        <div style={{
            justifyContent: "center",
            display: "flex",
          }}>
          <Switch>
            <Route path="/" exact component={() => <Home />} />
            <Route path="/about" exact component={() => <About />} />
            <Route path="/books" exact component={() => <Books />} />
            <Route path="/authors" exact component={() => <Authors />} />
            <Route path="/libs" exact component={() => <Libs />} />
            <Route path="/posts" exact component={() => <Posts />}/>
            <Route
                path="/search/q=:q/model=:model" exact component={() => <Results />}
                // render={(props) => (
                //   <Search q={props.match.params.q} model={props.match.params.model} />
                // )}
              />
            <Route
              path="/books/:bookId"
              exact
              component={() => <SingleBook />}
            />
            <Route
              path="/authors/:authId"
              exact
              component={() => <SingleAuthor />}
            />
            <Route
              path="/libs/:libId"
              exact
              component={() => <SingleLib />}
            />

            <Route path="/ourData" exact component={() => <OurData />}/>
            <Route path="/provData" exact component={() => <ProviderData />}/>
          </Switch>
        </div>
        </QueryParamProvider> 
      </Router>
    </div>
  );
}

export default App;
