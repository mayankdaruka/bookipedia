import React, { useState, useEffect } from "react";
import { Row, Col, Pagination, Container, Spinner } from "react-bootstrap";
import { AuthorCard } from "./AuthorCard";
import styles from "./author.module.css";
import { Link } from "react-router-dom";
import { Grid } from "@material-ui/core";
import axios from "axios";
import { SearchBar } from "../../components/SearchBar";

// For changing query params
import {
  NumberParam,
  StringParam,
  useQueryParams,
  withDefault,
} from "use-query-params";

import { updateParams, updateParamsPageNum } from "../../helper/functions";
import { DropdownFilter } from "../../components/DropdownFilter";

const authsPerPage = 20;
const Authors = () => {
  const [authorData, setAuths] = useState([]);
  const [total_pages, setTotalPages] = useState(1);
  const [loading, setLoading] = useState(true);
  //const total_pages = authorData.length % authsPerPage === 0 ? Math.floor(authorData.length / authsPerPage) : Math.floor(authorData.length / authsPerPage) + 1;

  const param_names = [
    "first_name",
    "last_name",
    "average_book_ratings",
    "average_page_counts",
    "num_works",
    "sort_authors",
  ];

  // first_name: "First Name",
  // last_name: "Last Name",
  // average_book_ratings: "Avg. Books Ratings",
  // average_page_counts: "Avg. Book Length",
  // num_works: "Num Works",
  // sort_authors: "Sort Authors",

  // Setting up the params
  const [params, setParams] = useQueryParams({
    // sort: "Auther Name",
    // pageCount: 0,
    sort_authors: StringParam,
    page: withDefault(NumberParam, 1),
    first_name: StringParam,
    last_name: StringParam,
    average_book_ratings: StringParam,
    average_page_counts: StringParam,
    num_works: StringParam,
    // add a search as well later
  });

  useEffect(() => {
    const constructURLParams = (params) => {
      let URLParams = new URLSearchParams();
      URLParams.append("page", params.page); // we always want a page
      // If the user selected sort drop down, the value will appear
      if (params.sort_authors) {
        URLParams.append("sort", params.sort_authors);
      }

      if (params.first_name) {
        URLParams.append("first_name", params.first_name);
      }
      if (params.last_name) {
        URLParams.append("last_name", params.last_name);
      }
      if (params.average_book_ratings) {
        URLParams.append("average_book_ratings", params.average_book_ratings);
      }
      if (params.average_page_counts) {
        URLParams.append("average_page_counts", params.average_page_counts);
      }
      if (params.num_works) {
        URLParams.append("num_works", params.num_works);
      }

      return URLParams;
    };

    function getAuthorData() {
      setLoading(true);
      axios
        .get("https://api.bookipedia.me/api/authors", {
          params: constructURLParams(params),
        })
        .then((res) => {
          setAuths(res.data.authors);
          setTotalPages(res.data.total_pages);
          setLoading(false);
        });
    }

    getAuthorData();
    console.log(params);
  }, [params]);

  // {setBooks(res.data.books);setTotalPages(res.data.total_pages)}

  const [currentPage, setCurrentPage] = useState(params.page);
  // const pages = 12;
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
    // updateParams("page", pageNumber, setParams, params);
    updateParamsPageNum(pageNumber, setParams, params);
  };

  return (
    <div>
      <Container>
        <h1 className={styles.header}>Authors</h1>
        <p
          style={{
            justifyContent: "center",
            display: "flex",
            marginLeft: "200px",
            marginRight: "200px",
            marginBottom: "80px",
          }}
        >
          Authors use their amazing talents to create magical worlds. Explore
          this page to find out more about the people behind the books.
        </p>

        <div>
          <p style={{ justifyContent: "center", display: "flex" }}>
            Search through authors based on their name, biography, number of
            works, average book ratings and avergae book lengths
            <br />
            <br />
          </p>

          <SearchBar model={"authors"} />

          <Row
            style={{
              justifyContent: "center",
              display: "flex",
              marginLeft: "20px",
              marginBottom: "20px",
              marginTop: "30px",
            }}
          >
            {param_names.map((name) => (
              <Col>
                <DropdownFilter name={name} hook={[params, setParams]} hook2 ={[currentPage, setCurrentPage] }/>
              </Col>
            ))}

            {/* {console.log(params["sort"])}  for authors it looks through name, description, num works, average book ratings, average page counts
for libraries it looks through county, region, libname, adname, address, ser pop, col size, ann circ
*/}
          </Row>
        </div>
        {loading ? (
          <Row style={{ justifyContent: "center", paddingTop: 30 }}>
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </Row>
        ) : (
          <Grid
            container
            spacing={8}
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
            {authorData.map((card) => (
              <Grid item xs={9} sm={3} md={3}>
                <Link
                  to={`/authors/${card.auth_id}`}
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <AuthorCard
                    description={card.description}
                    name={card.name}
                    works={card.works}
                    image={card.image}
                    authId={card.auth_id}
                    first_name={card.first_name}
                    last_name={card.last_name}
                    average_book_ratings={card.average_book_ratings}
                    average_page_counts={card.average_page_counts}
                  />
                </Link>
              </Grid>
            ))}
          </Grid>
        )}
        <br />

        <p className={styles.header}>
          {/* Total instances: {currentAuthors.length} /  {authorData.length} <br /> */}
          Total pages: {total_pages}
        </p>

        {/* Pagination code */}

        <Pagination className={styles.pagination}>
          <Pagination.First onClick={() => paginate(1)} />
          <Pagination.Prev
            onClick={() => paginate(currentPage - 1 < 1 ? 1 : currentPage - 1)}
          />

          <Pagination.Item active>
            {" "}
            {currentPage < 1 ? 1 : currentPage}
          </Pagination.Item>

          <Pagination.Next
            onClick={() =>
              paginate(
                currentPage + 1 > total_pages ? total_pages : currentPage + 1
              )
            }
          />

          <Pagination.Last onClick={() => paginate(total_pages)} />
        </Pagination>
      </Container>
    </div>
  );
};

export default Authors;
