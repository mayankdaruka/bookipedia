import jkImg from "../../assets/authorCovers/jkrowling.jpg";
import pauloImg from "../../assets/authorCovers/paulo.jpg";
import brownImg from "../../assets/authorCovers/danBrown.jpg";

const authorData = [
  {
    nationality: "British",
    gender: "Female",
    name: "J. K. Rowling",
    age: 56,
    language: "English",
    numBook: 15,
    maxSales: 500000000,
    image: jkImg,
    description: "Joanne Rowling (born 31 July 1965), better known by her pen name J. K. Rowling, is a British author, philanthropist, film producer, television producer,",
    works: [1,2],

    //lib_id: 1
    id: 1,
  },
  {
    nationality: "Brazilian",
    gender: "Male",
    name: "Paulo Coelho",
    age: 74,
    language: "Portuguese",
    numBook: 28,
    maxSales: 350000000,
    image: pauloImg,
    description: "Joanne Rowling (born 31 July 1965), better known by her pen name J. K. Rowling, is a British author, philanthropist, film producer, television producer,",
    works: [1,2],
    id: 2,
  },
  {
    nationality: "American",
    gender: "Male",
    name: "Dan Brown",
    age: 57,
    language: "English",
    numBook: 7,
    maxSales: 200000000,
    image: brownImg,
    description: "Joanne Rowling (born 31 July 1965), better known by her pen name J. K. Rowling, is a British author, philanthropist, film producer, television producer,",
    works: [1,2],
    id: 3,
  },
];
export default authorData;
