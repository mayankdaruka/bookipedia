import {  Card } from "react-bootstrap";
// import parse from 'html-react-parser';
import styles from "./author.module.css";





export const AuthorCard = ({
  name,
  description,
  works,
  image,
  first_name,
  last_name,
  average_book_ratings,
  average_page_counts,

}) => {

  
  return (
    <Card className={styles.tool_card}>
      <Card.Img
        src={image}
        style={{ height: "10rem" }}
        fluid
        />
  
        <Card.Title style={{ paddingTop: 8 }}> {name} </Card.Title>
        <Card.Body>

        <text className={styles.fontStype}>First Name</text>: {first_name} <br />
        <text className={styles.fontStype}>Last Name</text>: {last_name} <br />
        <text className={styles.fontStype}>Average Rating of books</text>: {Math.round(average_book_ratings * 100) / 100} <br />
        <text className={styles.fontStype}>Average Book Length</text>: {average_page_counts} pages <br />
        <text className={styles.fontStype}>Number of books published</text>: {works.length} <br />

        <br />
      </Card.Body>
    </Card>
  );
};
