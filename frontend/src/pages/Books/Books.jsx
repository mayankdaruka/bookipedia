import React, { useState, useEffect, useRef } from "react";
import { Spinner, Row, Col, Pagination, Container } from "react-bootstrap";
import styles from "./book.module.css";
import { BookCard } from "./BookCard";
import { Link } from "react-router-dom";
import axios from "axios";
import { Grid } from "@material-ui/core";
import { SearchBar } from "../../components/SearchBar";

// For changing query params
import {
  ArrayParam,
  NumberParam,
  StringParam,
  useQueryParams,
  withDefault,
} from "use-query-params";

import { updateParams, updateParamsPageNum } from "../../helper/functions";
import { DropdownFilter } from "../../components/DropdownFilter";
const booksPerPage = 20;
const Books = () => {
  const [bookData, setBooks] = useState([]); // we will change this to books
  const [loading, setLoading] = useState(true); //change to true
  const [total_pages, setTotalPages] = useState(1);

  // const currentPage = useRef();
  // function setCurrentPage(pageNumber){
  //   currentPage = pageNumber;
  // }
  // const [currentPage, setCurrentPage] = useRef();

  // Setting up the params
  const [params, setParams] = useQueryParams({
    // sort: "Auther Name",
    // pageCount: 0,
    sort_books: StringParam,
    page: withDefault(NumberParam, 1),
    author_name: StringParam,
    price: StringParam,
    page_count: StringParam,
    average_rating: StringParam,
    published_year: StringParam,
    // add a search as well later
  });

  const [currentPage, setCurrentPage] = useState(params.page);

  // API Calls
  // const url = 'https://api.bookipedia.me/api/books'
  // useEffect(() => {
  //   //console.log("hello")
  //   axios.get(url).then(res =>
  //     setBooks(res.data.books)).then(() => setLoading(false))

  // }, []);

  useEffect(() => {
    const constructURLParams = (params) => {
      let URLParams = new URLSearchParams();
      URLParams.append("page", params.page); // we always want a page
      // If the user selected sort drop down, the value will appear
      if (params.sort_books) {
        URLParams.append("sort", params.sort_books);
      }

      if (params.author_name) {
        URLParams.append("author_name", params.author_name);
      }

      if (params.price) {
        URLParams.append("price", params.price);
      }
      if (params.page_count) {
        URLParams.append("page_count", params.page_count);
      }
      if (params.average_rating) {
        URLParams.append("average_rating", params.average_rating);
      }
      if (params.published_year) {
        URLParams.append("published_year", params.published_year);
      }
      return URLParams;
    };

    function getBookData() {
      setLoading(true);
      axios
        .get("https://api.bookipedia.me/api/books", {
          params: constructURLParams(params),
        })
        .then((res) => {
          setBooks(res.data.books);
          setTotalPages(res.data.total_pages);
        })
        .then(() => setLoading(false));
    }

    getBookData();
  }, [params, currentPage]);

  // Pagination
  // Change page, when this updates,the next time it renders the above will get a diff slice
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
    // updateParams("page", pageNumber, setParams, params);
    updateParamsPageNum(pageNumber, setParams, params);
  };

  return (
    <div>
      <Container style={{ marginRight: "40px" }}>
        <h1 className={styles.header}>Books</h1>
        <p
          style={{
            justifyContent: "center",
            display: "flex",
            marginLeft: "200px",
            marginRight: "200px",
            marginBottom: "80px",
          }}
        >
          “Reading is essential for those who seek to rise above the ordinary.”
          - Jim Rohn <br />
          <br />
          Explore this page to find new books to read based on your interests or
          learn more about books you’ve already read! You can sort and filter
          books by authors, price, page count, average rating and published
          date!
        </p>
        <div>
          <p style={{ justifyContent: "center", display: "flex" }}>
            Search for books based on their title, author name, language,
            publisher and subtitle
            <br />
            <br />
          </p>

          <SearchBar model={"books"} />

          <Row
            style={{
              justifyContent: "center",
              display: "flex",
              marginLeft: "20px",
              marginBottom: "20px",
              marginTop: "30px",
            }}
          >
            {/* direction="row"
                    justify="flex-start"
                    alignItems="flex-start"> 
                        CHANGE AUTHOR NAME TO WHAT IS ON BACKEND,
                        // 'year', 'average_rating', 'price', 'num_ratings', 'page_count' (what it is called on backend)*/}
            {[
              "author_name",
              "price",
              "page_count",
              "average_rating",
              "published_year",
              "sort_books",
            ].map((name) => (
              <Col>
                <DropdownFilter
                  name={name}
                  // {...console.log(name)}
                  //value={params[name]}
                  hook={[params, setParams]}
                  hook2 ={[currentPage, setCurrentPage]}
                />
              </Col>
            ))}

            {/* {console.log(params["sort"])} */}
          </Row>
          {/* <p style={{ justifyContent: "center", display: "flex", }} >
          Search for books based on their title, author name, language, publisher and subtitle<br />
          <br />
        </p> */}

          {/* <SearchBar model={"books"} /> */}
        </div>

        <div>
          {loading ? (
            <Row style={{ justifyContent: "center", paddingTop: 30 }}>
              <Spinner animation="border" role="status">
                <span className="visually-hidden">Loading...</span>
              </Spinner>
            </Row>
          ) : (
            <Grid
              container
              spacing={10}
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
            >
              {bookData.map((card) => (
                <Grid item xs={9} sm={3} md={3}>
                  <Link
                    to={`/books/${card.book_id}`}
                    style={{ textDecoration: "none", color: "inherit" }}
                  >
                    <BookCard
                      title={card.title}
                      authors={card.authors}
                      price={card.price}
                      currency={card.currency}
                      page_count={card.page_count}
                      average_rating={card.average_rating}
                      published_date={card.published_date}
                      small_thumbnail={card.small_thumbnail}
                      // id={card.id}
                    />
                  </Link>
                </Grid>
              ))}
            </Grid>
          )}
        </div>
        <br />

        <p className={styles.header}>
          {/* Total instances: {bookData.length} /  {bookData.length} <br /> */}
          Total pages: {total_pages}
        </p>

        <Pagination className={styles.pagination}>
          <Pagination.First onClick={() => paginate(1)} />
          <Pagination.Prev
            onClick={() => paginate(currentPage - 1 < 1 ? 1 : currentPage - 1)}
          />

          <Pagination.Item active>
            {" "}
            {currentPage < 1 ? 1 : currentPage}
          </Pagination.Item>

          <Pagination.Next
            onClick={() =>
              paginate(
                currentPage + 1 > total_pages ? total_pages : currentPage + 1
              )
            }
          />

          <Pagination.Last onClick={() => paginate(total_pages)} />
        </Pagination>
      </Container>
    </div>
  );
};

export default Books;
