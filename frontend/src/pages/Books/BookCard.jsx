
 import styles from "./book.module.css";

import ReactCardFlip from 'react-card-flip'
import { useState } from "react";
import {enumerate} from "../../helper/functions"



export const BookCard = ({
  title,
  authors,
  price,
  currency,
  page_count,
  average_rating,
  published_date,
  small_thumbnail,
}) => {
  const [isFlipped, setIsFlipped] = useState(1);
  return (

    <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">

      <div
        onMouseOut={() => setIsFlipped((prev) => !prev)}
        className="CardBack"
        style={{
          width: '310px',
          height: '210px',
          backgroundColor: '#b8cae0',
          border: '4px solid rgba(0, 0, 0, 0.05)'
        }}
      >
        <p style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginLeft: "20px",
          marginRight: "20px",
        }}>
          <p style={{ marginTop: "10px" }}>
            <text className={styles.fontStype}>Title</text>: {title} <br />
            {/* To do: figure out how to go through author array
             J. K. RowlingPaulo Coelho
             */}
            <text className={styles.fontStype}>Authors</text>: {enumerate(authors)} <br />
            <text className={styles.fontStype}>Price</text>: {currency} {price === null ? 'none' : price} <br />
            <text className={styles.fontStype}>Page Count</text>: {page_count} <br />
            <text className={styles.fontStype}>Average Rating</text>: {average_rating} <br />
            <text className={styles.fontStype}>Date Published</text>: {published_date} <br />
          </p>
        </p>
      </div>

      <div
        onMouseOver={() => setIsFlipped((prev) => !prev)}
        className="CardFront"
        style={{
          width: '310px',
          height: '210px',
          backgroundColor: '#b8cae0',
          border: '4px solid rgba(0, 0, 0, 0.05)'
        }}
      >
        <div>
          <p style={{
            display: "flex",
            alignItems: "center",
            marginLeft: "10px",
            marginTop: "10px"
          }}>
            <img src={small_thumbnail} style={{
              height: '150px',
              width: '100px',
              marginRight: '20px',
              marginTop: "10px"
            }} alt="" />
            <text className={styles.fontStype}>
            {title}<br />
            By: {authors[0]}
            </text>
          </p>
        </div>
      </div>

    </ReactCardFlip>
    // <Card >
    //   <Card.Img
    //     src={image}
    //   />
    //   <Card.Title style={{ paddingTop: 10 }}> {name} </Card.Title>
    //   <Card.Body>
    //     Name: {name}<br />
    //     Author: {author} <br />
    //     Genre: {genre} <br />
    //     Language: {language} <br />
    //     Approximate Sales: {approxSales} <br />
    //     Date Published: {publishDate} <br />
    //     <br />
    //   </Card.Body>
    // </Card>
  );
};
