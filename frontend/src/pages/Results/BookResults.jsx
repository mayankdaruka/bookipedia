import React, { useState } from "react";
import styles from "../Books/book.module.css";
import ReactCardFlip from 'react-card-flip'
import Highlighter from "react-highlight-words";
import { get_proper_name } from "../../helper/functions"



export const BookResults = ({
    searchQuery,
    name,
    image,
    authorName,
    language,
    publisher,
    subTitle
}) => {


    const [isFlipped, setIsFlipped] = useState(1);


    return (

      <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">

      <div
        onMouseOut={() => setIsFlipped((prev) => !prev)}
        className="CardBack"
        style={{
          width: '310px',
          height: '210px',
          backgroundColor: '#b8cae0',
          border: '4px solid rgba(0, 0, 0, 0.05)'
        }}
      >
        <p style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginLeft: "20px",
          marginRight: "20px",
        }}>
          <p style={{ marginTop: "10px" }}>
          <text className={styles.fontStype}>Book Name</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${name}`}
                    />
                }
                <br />
                <text className={styles.fontStype}>Sub Title</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${subTitle}`}
                    />
                } <br />
                <text className={styles.fontStype}>Author Name</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${authorName}`}
                    />
                } <br />
                <text className={styles.fontStype}>Language</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${get_proper_name(language)}`}
                    />
                } <br />
                <text className={styles.fontStype}>Publisher</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${publisher}`}
                    />
                } <br />

                <br />
          </p>
        </p>
      </div>

      <div
        onMouseOver={() => setIsFlipped((prev) => !prev)}
        className="CardFront"
        style={{
          width: '310px',
          height: '210px',
          backgroundColor: '#b8cae0',
          border: '4px solid rgba(0, 0, 0, 0.05)'
        }}
      >
        <div>
          <p style={{
            display: "flex",
            alignItems: "center",
            marginLeft: "10px",
            marginTop: "10px"
          }}>
            <img src={image} style={{
              height: '150px',
              width: '100px',
              marginRight: '20px',
              marginTop: "10px"
            }} alt="" />
            <text className={styles.fontStype}>
            {name}<br />
            By: {authorName}
            </text>
          </p>
        </div>
      </div>

    </ReactCardFlip>
    );
};
