import React, { useState, useEffect } from 'react';
import { useParams, Link } from "react-router-dom";
import { Grid } from "@material-ui/core"
import axios from "axios";

import {  Container } from "react-bootstrap";
import { updateParams } from "../../helper/functions"
import MyPagination from "../../components/MyPagination"

// For display the search results
import { BookResults } from "./BookResults";
import { AuthorResults } from "./AuthorResults";
import { LibResults } from "./LibResults";


import styles from "./results.module.css";

import {
  StringParam,
  useQueryParams,
  NumberParam,
  withDefault,
} from "use-query-params"



const Results = () => {
  const { q } = useParams();
  const { model } = useParams();
  let query_array = String(q).split(" ");
  console.log("query", query_array)
  const [bookData, setBookData] = useState([])
  const [authorData, setAuthorsData] = useState([])
  const [libData, setLibData] = useState([])
  // bookData, authorData, libData


  // Setting up the params
  const [params, setParams] = useQueryParams({

    books_page: withDefault(NumberParam, 1),
    authors_page: withDefault(NumberParam, 1),
    lib_page: withDefault(NumberParam, 1),

  });
//pagination logic
  const [bookCurrentPage, setBookCurrentPage] = useState(params.books_page);
  const [authCurrentPage, setAuthCurrentPage] = useState(params.authors_page);
  const [libCurrentPage, setLibCurrentPage] = useState(params.lib_page);
  const [bookTotalPage, setBookTotalPage] = useState(1);
  const [authTotalPage, setAuthTotalPage] = useState(1);
  const [libTotalPage, setLibTotalPage] = useState(1);
  const total_pages = 20;

  const books_paginate = (pageNumber) => { setBookCurrentPage(pageNumber); updateParams("books_page", pageNumber, setParams, params) }
  const authors_paginate = (pageNumber) => { setAuthCurrentPage(pageNumber); updateParams("authors_page", pageNumber, setParams, params) }
  const libs_paginate = (pageNumber) => { setLibCurrentPage(pageNumber); updateParams("lib_page", pageNumber, setParams, params) }

  useEffect(() => {
    const constructBookURLParams = () => {
      let URLParams = new URLSearchParams()
      URLParams.append("search", q) // we always want a page
      if (params.books_page) {
        URLParams.append("page", params.books_page)
      }

      return URLParams
    }

    const constructAuthorsURLParams = () => {
      let URLParams = new URLSearchParams()
      URLParams.append("search", q) // we always want a page

      if (params.authors_page) {
        URLParams.append("page", params.authors_page)
      }
      return URLParams
    }

    const constructLibsURLParams = () => {
      let URLParams = new URLSearchParams()
      URLParams.append("search", q) // we always want a page

      if (params.lib_page) {
        URLParams.append("page", params.lib_page)
      }

      return URLParams
    }

    function getBookData() {
      axios.get(`https://api.bookipedia.me/api/books`, {
        params: constructBookURLParams(params),
      }).then(res => {setBookData(res.data.books); ;setBookTotalPage(res.data.total_pages)})
    }


    function getAuthorData() {
      axios.get('https://api.bookipedia.me/api/authors', {
        params: constructAuthorsURLParams(params),
      }).then(res => {setAuthorsData(res.data.authors); setAuthTotalPage(res.data.total_pages)})
    }

    function getLibData() {
      axios.get('https://api.bookipedia.me/api/libraries', {
        params: constructLibsURLParams(params),
      }).then(res => {setLibData(res.data.libraries); setLibTotalPage(res.data.total_pages)})
    }

    if (String(model) === "books" || String(model) === "all") {
      getBookData();
    }

    if (String(model) === "authors" || String(model) === "all") {
      getAuthorData();
    }

    if (String(model) === "libraries" || String(model) === "all") {
      getLibData();
    }

    // console.log(params);
  }, [params])


  console.log(bookData)



  const switchData = (modelName) => {


    if (modelName === "books") {
      return (
        <>

          <h1
            className={styles.paragraph}>
            Books Results
          </h1>
          <Grid container spacing={10}
            direction="row"
            alignItems="center"
            justify="center">
            {
              bookData.map((card) => (
                <Grid item xs={9} sm={3} md={3}>
                  <Link to={`/books/${card.book_id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                    <BookResults style={{ marginRight: "80px" }}
                      name={card.title}
                      authorName={card.author_name}
                      language={card.language}
                      publisher={card.publisher}
                      subTitle={card.subtitle}
                      image={card.small_thumbnail}
                      searchQuery={query_array}
                    // id={card.id}
                    />
                  </Link>
                </Grid>
              ))
            }
          </Grid>

          <MyPagination
            currentPage={bookCurrentPage}
            total_pages = {bookTotalPage}
            first={() => books_paginate(1)}
            prev={() => books_paginate(bookCurrentPage - 1 < 1 ? 1 : bookCurrentPage - 1)}
            next={() => books_paginate(bookCurrentPage + 1 > bookTotalPage ? bookTotalPage : bookCurrentPage + 1)}
            last={() => books_paginate(bookTotalPage)} />

        </>
      )
    }


    if (modelName === "authors") {
      return (
        <>
          <h1
            className={styles.paragraph}>
            Author Results
          </h1>
          <Grid container spacing={8}
            direction="row"
            justify="flex-start"
            alignItems="flex-start">
            {authorData.map((card) => (
              <Grid item xs={9} sm={3} md={3}>
                <Link to={`/authors/${card.auth_id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                  <AuthorResults
                    authorName={card.name}
                    numBooks={card.num_works}
                    image={card.image}
                    rating={card.average_book_ratings}
                    bookLength={card.average_page_counts}
                    searchQuery={query_array}
                  />
                </Link>
              </Grid>
            ))}
          </Grid>
          <MyPagination
            currentPage={authCurrentPage}
            total_pages = {authTotalPage}
            first={() => authors_paginate(1)}
            prev={() => authors_paginate(authCurrentPage - 1 < 1 ? 1 : authCurrentPage - 1)}
            next={() => authors_paginate(authCurrentPage + 1 > authTotalPage ? authTotalPage : authCurrentPage + 1)}
            last={() => authors_paginate(authTotalPage)} />
        </>
      )
    }



    if (modelName === "libraries") {
      return (
        <>
          <h1
            className={styles.paragraph}>
            Library Results
          </h1>
          <Grid container spacing={8}
            direction="row"
            justify="flex-start"
            alignItems="flex-start">
            {libData.map((card) => (
              <Grid item xs={9} sm={3} md={3}>
                <Link to={`/libs/${card.id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                  <LibResults
                    libName={card.libname}
                    county={card.county}
                    region={card.region}
                    administrator={card.adname}
                    address={card.address}
                    image={card.img_link}
                    searchQuery={query_array}
                  />
                </Link>
              </Grid>
            ))}
          </Grid>
          <MyPagination
            currentPage={libCurrentPage}
            total_pages = {libTotalPage}
            first={() => libs_paginate(1)}
            prev={() => libs_paginate(libCurrentPage - 1 < 1 ? 1 : libCurrentPage - 1)}
            next={() => libs_paginate(libCurrentPage + 1 > libTotalPage ? libTotalPage : libCurrentPage + 1)}
            last={() => libs_paginate(libTotalPage)} />

        </>


      )
    }

    if (modelName === "all") {
      return (
        <>
          <h1
            className={styles.paragraph}>
            Book Results
          </h1>
          <Grid container spacing={10}
            direction="row"
            alignItems="center"
            justify="center">
            {
              bookData.map((card) => (
                <Grid item xs={9} sm={3} md={3}>
                  <Link to={`/books/${card.book_id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                    <BookResults style={{ marginRight: "80px" }}
                      name={card.title}
                      authorName={card.author_name}
                      language={card.language}
                      publisher={card.publisher}
                      subTitle={card.subtitle}
                      image={card.small_thumbnail}
                      searchQuery={query_array}
                    // id={card.id}
                    />
                  </Link>
                </Grid>
              ))
            }
          </Grid>

          <MyPagination
            currentPage={bookCurrentPage}
            total_pages = {bookTotalPage}
            first={() => books_paginate(1)}
            prev={() => books_paginate(bookCurrentPage - 1 < 1 ? 1 : bookCurrentPage - 1)}
            next={() => books_paginate(bookCurrentPage + 1 > bookTotalPage ? bookTotalPage : bookCurrentPage + 1)}
            last={() => books_paginate(bookTotalPage)} />

          <h1
            className={styles.paragraph}>
            Author Results
          </h1>
          <Grid container spacing={8}
            direction="row"
            justify="flex-start"
            alignItems="flex-start">
            {authorData.map((card) => (
              <Grid item xs={9} sm={3} md={3}>
                <Link to={`/authors/${card.auth_id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                  <AuthorResults
                    authorName={card.name}
                    numBooks={card.num_works}
                    image={card.image}
                    rating={card.average_book_ratings}
                    bookLength={card.average_page_counts}
                    searchQuery={query_array}
                  />
                </Link>
              </Grid>
            ))}
          </Grid>
          <MyPagination
            currentPage={authCurrentPage}
            total_pages = {authTotalPage}
            first={() => authors_paginate(1)}
            prev={() => authors_paginate(authCurrentPage - 1 < 1 ? 1 : authCurrentPage - 1)}
            next={() => authors_paginate(authCurrentPage + 1 > authTotalPage ? authTotalPage : authCurrentPage + 1)}
            last={() => authors_paginate(authTotalPage)}/>

          <h1
            className={styles.paragraph}>
            Library Results
          </h1>
          <Grid container spacing={8}
            direction="row"
            justify="flex-start"
            alignItems="flex-start">
            {libData.map((card) => (
              <Grid item xs={9} sm={3} md={3}>
                <Link to={`/libs/${card.id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                  <LibResults
                    libName={card.libname}
                    county={card.county}
                    region={card.region}
                    administrator={card.adname}
                    address={card.address}
                    image={card.img_link}
                    searchQuery={query_array}
                  />
                </Link>
              </Grid>
            ))}
          </Grid>
          <MyPagination
             currentPage={libCurrentPage}
             total_pages = {libTotalPage}
             first={() => libs_paginate(1)}
             prev={() => libs_paginate(libCurrentPage - 1 < 1 ? 1 : libCurrentPage - 1)}
             next={() => libs_paginate(libCurrentPage + 1 > libTotalPage ? libTotalPage : libCurrentPage + 1)}
             last={() => libs_paginate(libTotalPage)}  />
        </>
      )
    }


  }


  return (
    <Container>

      <div>


        {
          switchData(`${model}`)
        }


      </div>

    </Container>
  )
};

export default Results;
