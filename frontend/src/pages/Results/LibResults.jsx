import { Card } from "react-bootstrap";
import  { useState } from "react";
import styles from "../Author/author.module.css";
import Highlighter from "react-highlight-words";




export const LibResults = ({
    searchQuery,
    image,
    libName,
    county,
    region,
    administrator,
    address
}) => {



    return (
        <Card className={styles.tool_card}>
            <Card.Img
                src={image}
                style={{ height: "10rem" }}
                fluid
            />

            <Card.Title style={{ paddingTop: 8 }}> {libName} </Card.Title>
            <Card.Body>
                <text className={styles.fontStype}>Library Name</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${libName}`}
                    />
                } <br />
                <text className={styles.fontStype}>County</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${county}`}
                    />
                } <br />
                <text className={styles.fontStype}>Region</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${region}`}
                    />
                } <br />
                <text className={styles.fontStype}>Administrator</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${administrator}`}
                    />
                } <br />
                <text className={styles.fontStype}>Library Address</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${address}`}
                    />
                } <br />

                <br />
            </Card.Body>
        </Card>
    );
};
