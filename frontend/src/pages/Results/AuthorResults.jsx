import { Card } from "react-bootstrap";
import { useState} from "react";
import styles from "../Author/author.module.css";
import Highlighter from "react-highlight-words";




export const AuthorResults = ({
    searchQuery,
    image,
    authorName,
    numBooks,
    rating,
    bookLength
}) => {




    return (
        <Card className={styles.tool_card}>
            <Card.Img
                src={image}
                style={{ height: "10rem" }}
                fluid
            />

            <Card.Title style={{ paddingTop: 8 }}> {authorName} </Card.Title>
            <Card.Body>
                <text className={styles.fontStype}>Author Name</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${authorName}`}
                    />
                } <br />
                <text className={styles.fontStype}>Number of Books</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${numBooks}`}
                    />
                } <br />
                <text className={styles.fontStype}>Avg Rating</text>:{
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${Math.round(rating * 100) / 100}`}
                    />
                }<br />
                <text className={styles.fontStype}>Avg Book Length</text>: {
                    <Highlighter
                        highlightClassName={styles.highlight}
                        searchWords={searchQuery}
                        textToHighlight={`${bookLength}`}
                    />
                }  <br />
                {/* <text className={styles.fontStype}>Publisher</text>: {publisher} <br /> */}

                <br />
            </Card.Body>
        </Card>
    );
};
