import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import styles from "./singleLib.module.css";

import { Button, Table, Row, Col, Container, Spinner } from "react-bootstrap";
import axios from "axios";
import { Grid } from "@material-ui/core";
import { BookCard } from "../Books/BookCard";
import { MyPagination } from "../../components/MyPagination";
import { BookList } from "../../components/BookList";
import { AuthorList } from "../../components/AuthorList";

function get_map(lat, lng) {
  let lat_s = lat + "";
  let lng_s = lng + "";

  let mapsrc =
    "https://maps.google.com/maps?q=" +
    lat_s +
    ", " +
    lng_s +
    "&z=15&output=embed";

  return mapsrc;
}

const SingleLib = ({ libData }) => {
  const { libId } = useParams();
  const [thisLib, setLib] = useState([]);
  const [bookData, setBooks] = useState([]);
  const [authorData, setAuths] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get(`https://api.bookipedia.me/api/library/${libId}`).then((res) => {
      setLib(res.data.library);
      setBooks(res.data.library_books);
      setAuths(res.data.library_authors);
      setLoading(false);
    });
  }, [libId]);

  return (
    <div>
      {loading ? (
        <Row style={{ justifyContent: "center", paddingTop: 30 }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </Row>
      ) : (
        <Container>
          <p className={styles.paragraph}>
            <h1> {thisLib.libname}</h1>
          </p>

          <p className={styles.paragraph} style={{ marginLeft: "100px" }}>
            <img
              src={thisLib.img_link}
              style={{
                marginLeft: "100px",
                height: "300px",
                wiidth: "300px",
                marginRight: "100px",
              }}
            />
            <text className={styles.fontStype}>
              Address: {thisLib.address} <br />
              County: {thisLib.county} <br />
              Region: {thisLib.region} <br />
              Phone Number: {thisLib.phone} <br />
              Administrators Name: {thisLib.adname} <br />
            </text>
          </p>

          <p style={{ marginLeft: "270px" }}>
            <Button
              style={{ border: "none", backgroundColor: "purple" }}
              href={thisLib.liburl}
            >
              Go to Library website
            </Button>{" "}
            <br />
          </p>

          <div>
            <div>
              <text
                className={styles.fontStype}
                style={{
                  fontSize: "22px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: "50px",
                }}
              >
                Books Available at this library
              </text>
              {loading ? (
                <Row style={{ justifyContent: "center", paddingTop: 30 }}>
                  <Spinner animation="border" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </Spinner>
                </Row>
              ) : (
                <Grid
                  container
                  spacing={10}
                  direction="row"
                  alignItems="center"
                  justify="center"
                >
                  {bookData.map((card) => (
                    <Grid item xs={9} sm={3} md={3}>
                      <Link
                        to={`/books/${card.book_id}`}
                        style={{ textDecoration: "none", color: "inherit" }}
                      >
                        <BookCard
                          style={{ marginRight: "80px" }}
                          title={card.title}
                          authors={card.authors}
                          price={card.price}
                          currency={card.currency}
                          page_count={card.page_count}
                          average_rating={card.average_rating}
                          published_date={card.published_date}
                          small_thumbnail={card.small_thumbnail}
                          // id={card.id}
                        />
                      </Link>
                    </Grid>
                  ))}
                </Grid>
              )}
            </div>

            <div>
              <h1
                className={styles.fontStype}
                style={{
                  fontSize: "22px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: "50px",
                }}
              >
                List of Authors available at this libray
              </h1>

              <AuthorList authors={authorData} />
            </div>
          </div>
          <p className={styles.map}>
            <iframe
              style={{ width: 600, height: 450, border: 0 }}
              src={get_map(thisLib.lat, thisLib.lng)}
              allowfullscreen=""
              loading="lazy"
            ></iframe>
          </p>
        </Container>
      )}
    </div>
  );
};

export default SingleLib;
