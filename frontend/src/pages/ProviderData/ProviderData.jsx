import React from "react";
// import pieChart from "./Charts/pieChart";
import CityChart from "./Charts/CityChart";
import FlightsChart from "./Charts/FlightsChart";
import CovidChart from "./Charts/CovidChart";


const ProviderData = () => {



  return (
    <div>
       {/* <h style={{ justifyContent: "center", display: "flex", marginButton: "40px" }}>Our Provider Visualization</h> */}
       <CityChart />
       <br />
       <FlightsChart />
       <br />
       <CovidChart />
       <br />
    </div>
  )

};

export default ProviderData;