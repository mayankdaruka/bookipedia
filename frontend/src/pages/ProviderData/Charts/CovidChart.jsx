import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Row, Spinner } from "react-bootstrap";


import {
    ScatterChart,
    Scatter,
    XAxis,
    YAxis,
    ZAxis,
    CartesianGrid,
    Tooltip,
    Cell,
} from 'recharts';

//   import { scaleOrdinal } from 'd3-scale';
// import { schemeCategory10 } from 'd3-scale-chromatic'; 
//   const colors = scaleOrdinal(schemeCategory10).range();
const COLORS = [
    "#a70957",
    "#a75909",
    "#09a759",
    "#0957a7",
    // "#A9D1F7",
    // "#CC99FF",
];



// Add loading indicators if time 
const CovidChart = () => {
    const [covidData, setCovid] = useState([]);
    const [loading, setLoading] = useState(true);
    const [covidMappings, setCovidMappings] = useState([]);

    const history = useHistory();
    const url = 'https://api.atraveltx.me/api/covid'
    useEffect(() => {
        axios.get(url).then(res =>{
            setCovid(res.data.result);
        getData(res.data.result)}).then(() => setLoading(false))
    }, []);

    function getData(covidData){
    var covidMappings = [];
    for (let i = 0; i < covidData.length; i++) {
        if (covidData[i]['deaths'] < 2500) {
            covidMappings.push({
                name: covidData[i]['county_name'],
                deaths: covidData[i]['deaths'],
                infection_rate: covidData[i]['infection_rate']
            });
        }
    }

    setCovidMappings(covidMappings);
}





    //const regionMapping = Object.keys(dict).map((key) => {return{region: key, libs: dict[key]}});
    console.log(covidMappings);


    return (
        <div>
            <h2 style={{ justifyContent: "center", display: "flex" }}>Number of Deaths vs Infection Rate</h2>
       { loading ? (
      <Row style={{ justifyContent: "center", paddingTop: 30 }}>
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Row>
      ) : (
        <div  >

            <ScatterChart
                width={1000}
                height={800}
                data={covidMappings}
            >
                <CartesianGrid />
                <XAxis type="number" dataKey="infection_rate" name="Infection_rate" label={{
                    value: "Infection_rate",
                    position: "insideBottom",
                    offset: -5,
                }}
                domain={[0.4, 1.5]}
                    tickCount={4} />
                <YAxis type="number" dataKey="deaths" name="Deaths" domain={[0, 2000]} label={{
                    value: "Deaths",
                    angle: -90,
                    position: "right",
                    offset: -55,
                }} />
                <ZAxis type="category" dataKey="name" name="County" />
                <Scatter data={covidMappings} fill="#8884d8">
                    {covidMappings.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                    ))}
                </Scatter>

                <Tooltip labelFormatter={() => { return ''; }} cursor={{ strokeDasharray: '3 3' }} />

            </ScatterChart>

        </div>
         )}
         </div>

    )

};

export default CovidChart;