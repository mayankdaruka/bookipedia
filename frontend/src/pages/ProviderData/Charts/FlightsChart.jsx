import React, { useState, useEffect } from "react";
import axios from "axios";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Cell, LabelList } from "recharts";
import MyPagination from "../../../components/MyPagination"
import { Row,  Spinner } from "react-bootstrap";

const COLORS = [
    "#072cc8",
    "#a407c8",
    // "#EBEA99",
    // "#B4F0A7",
    // "#A9D1F7",
    // "#CC99FF",
  ];
  
// Add loading indicators if time 
const FlightsChart = () => {
  const [flightsData, setFlights] = useState([]); 
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [pubMapping, setPubMapping] = useState([]);

  const url = 'https://api.atraveltx.me/api/flight'
  useEffect(() => {
    axios.get(url).then(res =>{
        setFlights(res.data.result);
        getData(res.data.result)}).then(() => setLoading(false))
  }, []);

  function getData(flightsData) {
  var dict = [];

 for (let i = 0; i < flightsData.length; i++) {
   if(flightsData[i]['airline_name'] != null ){
    let airline = flightsData[i]['airline_name'];
    if(dict.hasOwnProperty(airline)){
      dict[airline] =  dict[airline] +1;
    }else{
      dict[airline] =  1;
    }
   }  
}


const pubMapping = Object.keys(dict).map((key) => {return{airline: key, "Number of Flights": dict[key]}});
pubMapping.sort((a, b) =>
          a["Number of Flights"] > b["Number of Flights"] ? -1 : 1
        )
      setPubMapping(pubMapping);
  }
console.log(pubMapping);

// pubMapping
// const currentAiline = pubMapping.slice(0, 75);
// console.log(dict);
const ailinePerPage = 16
const indexOfLastItem = currentPage * ailinePerPage;
const indexOfFirstItem = indexOfLastItem - ailinePerPage;
const currentAiline = pubMapping.slice(indexOfFirstItem, indexOfLastItem);
const total_pages = pubMapping.length%ailinePerPage ===0? Math.floor(pubMapping.length/ailinePerPage): Math.floor(pubMapping.length/ailinePerPage) + 1;
console.log(currentAiline);
  // Change page, when this updates,the next time it renders the above will get a diff slice
const paginate = (pageNumber) => setCurrentPage(pageNumber);




    return (

      <div>
<h2 style={{ justifyContent: "center", display: "flex" }}>Number of flights per airline to Texas</h2>
     { loading ? (
    <Row style={{ justifyContent: "center", paddingTop: 30 }}>
      <Spinner animation="border" role="status">
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    </Row>
    ) : (
        <div  >


 
        <BarChart
            width={1000}
            height={700}
            data={currentAiline}
            // layout="vertical"
          >
            <CartesianGrid strokeDasharray="3 3" />

            {/* dataKey="name" textAnchor= "end" sclaeToFit="true" verticalAnchor= "start"  interval={0} angle= "-40" stroke="#8884d8"  */}
            <XAxis
              type="category"
              dataKey="airline"
              // height= "10"
              label={{
                value: "Airline",
                position: "insideBottom",
                offset: -5,    
              }}
            />
            <YAxis
              type="number"
              dataKey="Number of Flights"
              domain={[0, 170]}
              label={{
                value: "Number of Flights",
                position: 'insideLeft',
                angle: -90
              }}
            />

            <Tooltip />
            <Bar dataKey="Number of Flights" fill="#8884d8" >
            <LabelList dataKey="Number of Flights"  />
            {currentAiline.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            ))}
            </Bar >
          </BarChart> 

 <MyPagination
            currentPage={currentPage}
            total_pages = {total_pages}
            first={() => paginate(1)}
            prev={() => paginate(currentPage - 1 < 1 ? 1 : currentPage - 1)}
            next={() => paginate(currentPage + 1 > total_pages ? total_pages : currentPage + 1)}
            last={() => paginate(total_pages)} /> 

     
    </div>
        )}
        </div>

    )
  
  };
  
  export default FlightsChart;