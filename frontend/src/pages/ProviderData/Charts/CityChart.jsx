import React, { useState, useEffect } from "react";
import axios from "axios";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { Row,  Spinner } from "react-bootstrap";

const CityChart = () => {
    
    const [cityData, setCities] = useState([]); 
    const [loading, setLoading] = useState(true);
    const [popMapping, setPopMapping] = useState([]);

    const url = 'https://api.atraveltx.me/api/city'
    useEffect(() => {
      axios.get(url).then(res => {
          setCities(res.data.result);
          getData(res.data.result)}).then(() => setLoading(false))
    }, []);

    function getData(cityData){
      var population_dict = [];
      population_dict["0 - 5K"] = 0;
      population_dict["5K - 10K"] = 0;
      population_dict["10K - 50K"] = 0;
      population_dict["50K - 100K"] = 0;
      population_dict["Greater than 100K"] = 0;
  
     for (let i = 0; i < cityData.length; i++) {
       if(cityData[i]['population'] != null && cityData[i]['population'] >= 0 ){
          let pop = cityData[i]['population'];
          if(pop >= 0 && pop <5000){
              population_dict["0 - 5K"]  += 1;
          }
          else if (pop >= 5000 && pop < 10000){
              population_dict["5K - 10K"]  += 1;
          }
           else if (pop >= 10000 && pop < 50000){
              population_dict["10K - 50K"]  += 1;
          }
           else if (pop >= 50000 && pop < 100000){
              population_dict["50K - 100K"]  += 1;
          }
           else if (pop >= 100000 ){
              population_dict["Greater than 100K"]  += 1;
          }
  
       }  
  }
  
  const popMapping = Object.keys(population_dict).map((key) => {return{popRange: key, "Number of Cities": population_dict[key]}});
  
  setPopMapping(popMapping)
    }
   

console.log(popMapping)
  
  
  
  // //const regionMapping = Object.keys(dict).map((key) => {return{region: key, libs: dict[key]}});
  // console.log(authorMappings);
  
  
  
  
  
      return (
        
        <div  >
          <h2 style={{ justifyContent: "center", display: "flex" }}>Number of cities in each population range</h2>
          {loading ? (
        <Row style={{ justifyContent: "center", paddingTop: 30 }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </Row>
      ) : (
          <div  >
  
      
         <LineChart
            width={1000}
            height={700}
          data={popMapping}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="popRange"  
              label={{
                value: "Population Range",
                position: "insideBottom",
                offset: -3,    
              }} 
              padding={{ left: 30, right: 30 }}/>
          <YAxis  label={{
                value: " Number of Cities",
                position: 'insideLeft',
                angle: -90
              }}/>
          <Tooltip />
          <Line type="monotone" dataKey="Number of Cities" stroke="#8884d8" activeDot={{ r: 8 }} />

        </LineChart> 
       
      </div>
      )}
      </div>
      )
    
    };
    
    export default CityChart;