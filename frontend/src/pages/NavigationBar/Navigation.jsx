import React from "react";
import { Link, withRouter } from "react-router-dom";
import icon from "../../assets/icon.png";

function Navigation(props) {
  return (
    <div className="navigation">
      <nav class="navbar navbar-expand navbar-dark bg-dark">
        <div class="container">
          <Link class="navbar-brand" to="/" >
            {/* React Multi-Page Website */}
            <img src={icon}  width="50" height="50" />
            Bookipedia
          </Link>

  

          <div>

            
            <ul class="navbar-nav ml-auto">
              <li
                class={`nav-item  ${
                  props.location.pathname === "/" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/">
                  Home
                  <span class="sr-only">(current)</span>
                </Link>
              </li>
              <li
                class={`nav-item  ${
                  props.location.pathname === "/about" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/about">
                  About
                </Link>
              </li>
              <li
                class={`nav-item  ${
                  props.location.pathname === "/books" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/books">
                  Books
                </Link>
              </li>
              <li
                class={`nav-item  ${
                  props.location.pathname === "/authors" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/authors">
                  Authors
                </Link>
              </li>

              <li
                class={`nav-item  ${
                  props.location.pathname === "/libs" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/libs">
                  Libraries
                </Link>
              </li>
               <li
                class={`nav-item  ${
                  props.location.pathname === "/ourData" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/ourData">
                  Our Data
                </Link>
              </li>
              <li
                class={`nav-item  ${
                  props.location.pathname === "/provData" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/provData">
                  Provider Data
                </Link>
              </li> 
 
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default withRouter(Navigation);
