import React from "react";
import { Container, Navbar, Nav } from "react-bootstrap";

export const HomeScreen = () => {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        {" "}
        <Navbar.Brand> Bookipedia </Navbar.Brand>{" "}
        <Nav>
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#books">Books</Nav.Link>
          <Nav.Link href="#authors">Authors</Nav.Link>
          {/* <Nav.Link href="#publishing">Publishing Houses</Nav.Link> */}
        </Nav>
      </Container>
    </Navbar>
  );
};
