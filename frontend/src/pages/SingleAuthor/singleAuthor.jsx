import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import authorData from "./singleAuthorData";
import { Button, Table, Row, Col, Container, Spinner } from "react-bootstrap";
import styles from "./singleAuthor.module.css";
import parse from "html-react-parser";
import axios from "axios";
import {
  get_publisher,
  get_lang,
  get_gender,
  get_latest_book,
} from "../../helper/functions";

import { Grid } from "@material-ui/core";

import { BookCard } from "../Books/BookCard";
import { LibList } from "../../components/LibList";
import { BookList } from "../../components/BookList";
// related_libraries

function get_parsed(descr) {
  let parsed = "";
  parsed = descr === null ? "none" : parse(descr + "");
  return parsed;
}

const SingleAuthor = () => {
  const { authId } = useParams();
  //const thisAuthor = authorData.find((author) => author.id === Number(authId));

  const [thisAuthor, setAuth] = useState([]);
  const [books, setBooks] = useState([]);
  const [loading, setLoading] = useState(true);

  const [libData, setLibs] = useState([]);

  useEffect(() => {
    axios.get(`https://api.bookipedia.me/api/author/${authId}`).then((res) => {
      setAuth(res.data.author);
      setBooks(res.data.books_written);
      setLibs(res.data.related_libraries);
      setLoading(false);
    });
  }, [authId]);

  return (
    <div>
      {loading ? (
        <Row style={{ justifyContent: "center", paddingTop: 30 }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </Row>
      ) : (
        <Container>
          <h1 className={styles.header}>{thisAuthor.name}</h1>

          <p className={styles.info}>
            <img
              src={thisAuthor.image}
              style={{
                //marginLeft: "200px",
                height: "250px",
                marginRight: "80px",
                width: "200px",
                height: "300px",
              }}
            />
            {/* Description: {thisAuthor.description === null ? 'none' : parse(thisAuthor.description)}<br /> */}
            {/* {typeof(books)} */}
            Average ratings of books:{" "}
            {Math.round(thisAuthor.average_book_ratings * 100) / 100} <br />
            Publisher : {get_publisher(books)}
            <br />
            Books written in :{" " + get_lang(books)}
            <br />
            Gender :{" " + get_gender(get_parsed(thisAuthor.description))}
            <br />
            Latest Book :{" " + get_latest_book(books)}
            <br />
            Average Book Length: {thisAuthor.average_page_counts} pages <br />
          </p>

          <p className={styles.video}>
            {/* Description: {get_parsed(thisAuthor.description)} <br /> */}
            <iframe
              style={{ width: 560, height: 315 }}
              src={"https://www.youtube.com/embed/" + thisAuthor.video_url}
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </p>

          <div>
            <text
              className={styles.fontStype}
              style={{
                fontSize: "22px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: "50px",
              }}
            >
              Books Written by this Author{" "}
            </text>
            <BookList books={books} />
          </div>

          <div style={{ marginBottom: 50 }}>
            <text
              className={styles.fontStype}
              style={{
                fontSize: "22px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: "50px",
              }}
            >
              Libraries you can find this Author
            </text>
            <LibList libs={libData} />
          </div>
        </Container>
      )}
    </div>
  );
};
export default SingleAuthor;
