import jkImg from "../../assets/authorCovers/jkrowling.jpg";
import pauloImg from "../../assets/authorCovers/paulo.jpg";
import brownImg from "../../assets/authorCovers/danBrown.jpg";
import jkAuthor from "../../assets/bookCovers/HarryPotter.jpg";
import pauloAuthor from "../../assets/bookCovers/Alchemist.jpg";
import brownAuthor from "../../assets/bookCovers/Davinci.jpg";
import pubImg1 from "../../assets//pubHouse/bloomsburypublishing.jpg";
import pubImg3 from "../../assets//pubHouse/doubleday.png";
import pubImg2 from "../../assets//pubHouse/HarperCollins.jpeg";

const singleAuthorData = [
  {
    name: "J. K. Rowling",
    bio: "Joanne Rowling (born 31 July 1965), better known by her pen name J. K. Rowling, is a British author, philanthropist, film producer, television producer, and screenwriter. She is best known for writing the Harry Potter fantasy series, which has won multiple awards and sold more than 500 million copies, becoming the best-selling book series in history. The books are the basis of a popular film series, over which Rowling had overall approval on the scripts and was a producer on the final films. She also writes crime fiction under the pen name Robert Galbraith.",
    awards:
      "Rowling has received honorary degrees from the University of St Andrews, the University of Edinburgh, Edinburgh Napier University, the University of Exeter (which she attended), the University of Aberdeen, and Harvard University, where she spoke at the 2008 commencement ceremony.",
    notWorks: "Harry Potter series, Cormoran Strike series",
    latBook: "The Christmas Pig, 2021",
    genWrites:
      "Fantasy, drama, young adult fiction, tragic comedy, crime fiction",
    bookInfo:
      "One of her most famous book is Harry Potter and the Prisoner of Azkaban",
    pubInfo:
      "Bloomsbury publishing is the publishers of the Harry Potter series",
    vidsrc: "https://www.youtube.com/embed/hGn5qrg8Ds4",
    image: jkImg,
    image2: jkAuthor,
    image3: pubImg1,
    id: 1,
  },
  {
    name: "Paulo Coelho",
    bio: "Paulo Coelho was born in Rio de Janeiro, Brazil, and attended a Jesuit school. Coelho married artist Christina Oiticica in 1980. Together they had previously spent half the year in Rio de Janeiro and the other half in a country house in the Pyrenees Mountains of France, but now the couple reside permanently in Geneva, Switzerland.",
    awards: "Hans Christian Andersen Award, Denmark, 2007",
    notWorks: "The Alchemist",
    latBook: "Adultery, 2014",
    genWrites: "Drama, romance",
    bookInfo:
      "In 1987, Coelho wrote a new book, The Alchemist, over the course of one two-week spurt of creativity",
    pubInfo:
      "The Alchemist took off. HarperCollins decided to publish the book in 1994",
    vidsrc: "https://www.youtube.com/embed/Dem4MiKg4sM",
    image: pauloImg,
    image2: pauloAuthor,
    image3: pubImg2,
    id: 2,
  },
  {
    name: "Dan Brown",
    bio: "Dan Gerhard Brown was born on June 22, 1964, in Exeter, New Hampshire. He has a younger sister, Valerie and brother, Gregory. Brown attended Exeter's public schools until the ninth grade. He grew up on the campus of Phillips Exeter Academy, where his father, Richard G. Brown was a teacher of mathematics. His mother, Constance (née Gerhard), trained as a church organist and student of sacred music.",
    awards: "Goodreads Choice Awards Best Mystery & Thriller, 2013",
    notWorks:
      "Digital Fortress, Deception Point, Angels & Demons, The Da Vinci Code, Inferno",
    latBook: "Origin, 2017",
    genWrites: "Thriller, adventure, mystery, conspiracy",
    bookInfo:
      "His fourth novel, The Da Vinci Code, became a bestseller, going to the top of the New York Times Best Seller list during its first week of release in 2003",
    pubInfo: "Doubleday is the publisher of the Robert Langedon series",
    vidsrc: "https://www.youtube.com/embed/4TEfnncv-nU",
    image: brownImg,
    image2: brownAuthor,
    image3: pubImg3,
    id: 3,
  },
];

export default singleAuthorData;
