import React from "react";
import { useParams, Link } from "react-router-dom";
import authorData from "./singleAuthorData";
import { Button } from "react-bootstrap";

const SingleAuthor = () => {
  const { authId } = useParams();
  const thisAuthor = authorData.find((author) => author.id === Number(authId));
  console.log(authId, "me");
  // name: "Paulo Coelho",
  // bio: "Joanne Rowling (born 31 July 1965), better known by her pen name J. K. Rowling, is a British author, philanthropist, film producer, television producer, and screenwriter. She is best known for writing the Harry Potter fantasy series, which has won multiple awards and sold more than 500 million copies, becoming the best-selling book series in history. The books are the basis of a popular film series, over which Rowling had overall approval on the scripts and was a producer on the final films. She also writes crime fiction under the pen name Robert Galbraith.",
  // awards: "Rowling has received honorary degrees from the University of St Andrews, the University of Edinburgh, Edinburgh Napier University, the University of Exeter (which she attended), the University of Aberdeen, and Harvard University, where she spoke at the 2008 commencement ceremony.",
  // notWorks: "Harry Potter series, Cormoran Strike series",
  // latBook: "The Christmas Pig",
  // genWrites: "Fantasy, drama, young adult fiction, tragic comedy, crime fiction",
  // bookInfo: "One of her most famous book is Harry Potter and the Prisoner of Azkaban",
  // pubInfo: "Bloomsbury publishing is the publishers of the harry potter series",
  // image: pauloImg,
  // id: 2,
  return (
    <>
      <h1>{thisAuthor.name}</h1>
      <img src={thisAuthor.image} />
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}
      >
        Biography: {thisAuthor.bio}
      </p>
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}
      >
        Awards: {thisAuthor.awards}
      </p>
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}
      >
        Notable works: {thisAuthor.notWorks}
      </p>
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}
      >
        Latest book: {thisAuthor.latBook}
      </p>
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}
      >
        Genres writing: {thisAuthor.genWrites}
      </p>
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}
      >
        Book information: {thisAuthor.bookInfo}
        <Link to={`/singleBook/${thisAuthor.id}`}>
          <Button variant="info">View Book Details</Button>
        </Link>
      </p>
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}
      >
        Publishing House information: {thisAuthor.pubInfo}
        <Link to={`/singlePubHouse/${thisAuthor.id}`}>
          <Button variant="info">View Publishing House Details</Button>
        </Link>
      </p>
    </>
  );
};
export default SingleAuthor;
