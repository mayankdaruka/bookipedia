import { Card } from "react-bootstrap";
// import styles from "./book.module.css";


// This is our Json request
// "userId": 1,
// "id": 2,
// "title": "qui est esse",
// "body": 

// <Card key={post.id}>
// <Card.Img variant="top" src="holder.js/100px180" />
// <Card.Body>
//     <Card.Title>Post number: {post.id}</Card.Title>
//     <Card.Text>
//         Post:  {post.title}
//     </Card.Text>
//     <Button variant="primary">Go somewhere</Button>
// </Card.Body>

// </Card>

export const PostCard = ({
  userId,
  id,
  title,
  body,
}) => (
  <Card key={id}>
    <Card.Img variant="top" src="holder.js/100px180" />
    <Card.Title style={{ paddingTop: 10 }}> {title} </Card.Title>
    <Card.Body>
      User: {userId} <br />
      Body: {body} <br />
      <br />
    </Card.Body>
  </Card>
);