import React, { useState, useEffect } from "react";
import {  Spinner, Row } from "react-bootstrap";
import { PostCard } from "./PostCard";
import axios from "axios";
//import Pagination from '../components/Pagination';
// import Pagination from '../../components/Pagination'
import Getposts from './Getposts'
// import Pagination from './Pagination'
import {Grid} from "@material-ui/core"
import { makeStyles } from '@material-ui/core/styles'


const Posts = () => {
    const [posts, setPosts] = useState([]); // we will change this to books
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(12); //books per page?

    // useEffect(() =>
    // {
    //     if (posts && posts.length>0) setLoading(false)
    // }, [posts])

    //const [posts, setPosts] = useState([]); // we will change this to books
    const url = 'https://jsonplaceholder.typicode.com/posts'
    useEffect(() => {
        axios.get(url).then(res =>
                setPosts(res.data)).then(setLoading(false))
    },[]);

  
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

    // Change page, when this updates,the next time it renders the above will get a diff slice
    const paginate = pageNumber => setCurrentPage(pageNumber);

    return (
        <div>
            <h1 className='text-primary mb-3'>Books</h1>
            <p
                style={{
                    justifyContent: "center",
                    display: "flex",
                    marginLeft: "200px",
                    marginRight: "200px",
                    marginBottom: "80px",
                }}
            >
                “Reading is essential for those who seek to rise above the ordinary.” -
                Jim Rohn <br />
                <br />
                Explore this page to find new books to read based on your interests or
                learn more about books you’ve already read! You can sort and filter books
                by genre, authors, Publishing date, language and approximate sales.
            </p>
            {/* <Getposts posts={currentPosts} loading={loading} /> */}
            <div>
                {loading ? (
                    <Row style={{ justifyContent: "center" }}>
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </Row>
                ) : (
                    <Grid container spacing={3}
                    direction="row"
                    justify="flex-start"
                    alignItems="flex-start">
                        {currentPosts.map((post) => (
                            <Grid item xs={9} sm={3} md={3}>
                                <PostCard
                                    userId={post.userId}
                                    id={post.id}
                                    title={post.title}
                                    body={post.body}
                                />
                            </Grid>
                        ))}
                    </Grid>
                )}
            </div>
            {/* <Pagination
                itemsPerPage={postsPerPage}
                totalItems={posts.length}
                paginate={paginate}
            /> */}
        </div>
    );

}

export default Posts;

