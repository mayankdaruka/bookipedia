import React from 'react';
import {CardGroup, Row, Col } from "react-bootstrap";
import { PostCard } from "./PostCard";

// It takes in a couple props

/* <ul className='list-group mb-4'>
{posts.map(post => (
  <li key={post.id} className='list-group-item'>
    {post.title}
  </li>
))}
</ul> */

// {posts.map(post => (
//     <Card key={post.id}>
//         <Card.Img variant="top" src="holder.js/100px180" />
//         <Card.Body>
//             <Card.Title>Post number: {post.id}</Card.Title>
//             <Card.Text>
//                 Post:  {post.title}
//             </Card.Text>
//             <Button variant="primary">Go somewhere</Button>
//         </Card.Body>

//     </Card>
// ))}
const Getposts = ({ posts, loading }) => {
    if (loading) {
        // Makes sure it is done loading, we can put a spinner later
        return <h2>Loading...</h2>;
    }

    return (
        <div>
            <CardGroup as={Row }>
                {posts.map((post) => (
                    <Col >
                        <PostCard
                            userId={post.userId}
                            id={post.id}
                            title={post.title}
                            body={post.body}
                        />
                    </Col>
                ))}
            </CardGroup>
        </div>
    );
};

export default Getposts;