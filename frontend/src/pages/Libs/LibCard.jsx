import { Table } from "react-bootstrap"
import { Link } from "react-router-dom";


export const LibCard = ({name, region, county, serPop, colSize, annCir, id}) =>
(
    <Link to={`/libs/${id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
        <Table striped bordered hover size="sm" >
            <thead>
                <tr >
                    <th style={{width: '17%'}}>Name</th>
                    <th style={{width: '17%'}}>Region</th>
                    <th style={{width: '17%'}}>County</th>
                    <th style={{width: '17%'}}>Service Population</th>
                    <th style={{width: '17%'}}>Collection Size</th>
                    <th style={{width: '17%'}}>Annual Circulation</th>
                    {/* <th style={{width: '20%'}}>Link</th> */}
                </tr>
            </thead>
            <tbody>                <tr>
                        <td>{name}</td>
                        <td>{region}</td>
                        <td>{county}</td>
                        <td>{serPop}</td>
                        <td>{colSize}</td>
                        <td>{annCir}</td>
                        {/* <td> <Link to={`/singlePubHouse/${Id}`}><Button style={{border : "none", backgroundColor : "purple"}}>View Details</Button></Link></td> */}
                    </tr>
            </tbody>
        </Table>
    </Link>

)