import React, { useState, useEffect } from "react";
import {
  Table,
  Row,
  Col,
  Pagination,
  Container,
  Spinner,
} from "react-bootstrap";
import styles from "./lib.module.css";
import { LibCard } from "./LibCard";
import axios from "axios";
import { SearchBar } from "../../components/SearchBar";

// name, region, city, serPop, colSize, annCir, id

// For changing query params    ArrayParam,
import {
  NumberParam,
  StringParam,
  useQueryParams,
  ArrayParam,
  withDefault,
} from "use-query-params";

import { updateParams, updateParamsPageNum } from "../../helper/functions";
import { DropdownFilter } from "../../components/DropdownFilter";

const libsPerPage = 20;
const Libs = () => {
  const [libData, setLibs] = useState([]); // we will change this to authors
  const [total_pages, setTotalPages] = useState(1);
  const [loading, setLoading] = useState(true);

  const [params, setParams] = useQueryParams({
    sort_libs: StringParam,
    page: withDefault(NumberParam, 1),
    region: StringParam,
    county: StringParam,
    ser_pop: StringParam,
    col_size: StringParam,
    ann_circ: StringParam,
  });

  useEffect(() => {
    const constructURLParams = (params) => {
      let URLParams = new URLSearchParams();
      URLParams.append("page", params.page); // we always want a page
      // If the user selected sort drop down, the value will appear
      if (params.sort_libs) {
        URLParams.append("sort", params.sort_libs);
      }

      if (params.region) {
        URLParams.append("region", params.region);
      }
      if (params.county) {
        URLParams.append("county", params.county);
      }
      if (params.ser_pop) {
        URLParams.append("ser_pop", params.ser_pop);
      }
      if (params.col_size) {
        URLParams.append("col_size", params.col_size);
      }
      if (params.ann_circ) {
        URLParams.append("ann_circ", params.ann_circ);
      }

      return URLParams;
    };

    function getLibData() {
      setLoading(true);
      axios
        .get("https://api.bookipedia.me/api/libraries", {
          params: constructURLParams(params),
        })
        .then((res) => {
          setLibs(res.data.libraries);
          setTotalPages(res.data.total_pages);
          setLoading(false);
        });
    }

    getLibData();
    console.log(params);
  }, [params]);

  const [currentPage, setCurrentPage] = useState(params.page);
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
    // updateParams("page", pageNumber, setParams, params);
    updateParamsPageNum(pageNumber, setParams, params);
  };

  return (
    <div>
      <Container>
        <h1 className={styles.header}>Libraries</h1>
        <p
          style={{
            justifyContent: "center",
            display: "flex",
            marginLeft: "200px",
            marginRight: "200px",
            marginBottom: "80px",
          }}
        >
          A Library’s main purpose is to make books available to everyone.
          Explore this page to learn more about Libraries in your area and what
          books you can check out!
        </p>
        <div>
          <p style={{ justifyContent: "center", display: "flex" }}>
            Search for libraries based on their name, county, region,
            administrator name, and address
            <br />
            <br />
          </p>

          <SearchBar model={"libraries"} />

          <Row
            style={{
              justifyContent: "center",
              display: "flex",
              marginLeft: "20px",
              marginBottom: "20px",
              marginTop: "30px",
            }}
          >
            {[
              "region",
              "county",
              "ser_pop",
              "col_size",
              "ann_circ",
              "sort_libs",
            ].map((name) => (
              <Col>
                <DropdownFilter name={name} hook={[params, setParams]}   hook2 ={[currentPage, setCurrentPage]} />
              </Col>
            ))}

            {/* {console.log(params["sort"])} */}
          </Row>
        </div>

        {loading ? (
          <Row style={{ justifyContent: "center", paddingTop: 30 }}>
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </Row>
        ) : (
          <Table as={Row}>
            {libData.map((table) => (
              <Col>
                <LibCard
                  name={table.libname}
                  region={table.region}
                  county={table.county}
                  serPop={table.ser_pop}
                  colSize={table.col_size}
                  annCir={table.ann_circ}
                  id={table.id}
                />
              </Col>
            ))}
          </Table>
        )}

        {/* <Pagination
        itemsPerPage={libsPerPage}
        totalItems={libData.length}
        paginate={paginate}
      /> */}
        <br />
        <p className={styles.header}>
          {/* Total instances: {currentLibs.length} /  {libData.length}<br /> */}
          Total pages: {total_pages}
        </p>
        <Pagination className={styles.pagination}>
          <Pagination.First onClick={() => paginate(1)} />
          <Pagination.Prev
            onClick={() => paginate(currentPage - 1 < 1 ? 1 : currentPage - 1)}
          />

          <Pagination.Item active>
            {" "}
            {currentPage < 1 ? 1 : currentPage}
          </Pagination.Item>

          <Pagination.Next
            onClick={() =>
              paginate(
                currentPage + 1 > total_pages ? total_pages : currentPage + 1
              )
            }
          />

          <Pagination.Last onClick={() => paginate(total_pages)} />
        </Pagination>
      </Container>
    </div>
  );
};

export default Libs;
