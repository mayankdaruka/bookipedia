
// self.id = id
// self.libname = libname
// self.adname = adname
// self.address = address
// self.liburl = liburl
// self.phone = phone
// self.county = county
// self.region = region
// self.ser_pop = ser_pop
// self.col_size = col_size
// self.ann_circ = ann_circ
// self.lat = lat
// self.lng = lng
// self.img_link = img_link
// self.books = [0] * book_list_size
// self.authors = [0] * author_list_size

const libData = [
    { //model page
        libname: "Abernathy Public Library",
        region: "West Texas",
        city: "Abernathy",
        ser_pop: 3700,
        col_size: 9651,
        ann_circ: 4481,

        //instance page
        county: "Hale",
        address: "811 Avenue D .. ",
        phone: "806-298-2546",
        adname: "Jessica Stone",
        liburl: "https://abernathy.ploud.net/",

        //Other details
        desc: "Abarnathy Public Library is a public library, affiliated with ..",
        orgStruct: "This is a publicly funded and managed library",
        mapSrc: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3314.2427224669377!2d-101.84579544871754!3d33.831850580571555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86fe1b9515b0f61f%3A0x52625cdbd1ce38db!2sAbernathy%20Public%20Library!5e0!3m2!1sen!2sus!4v1634517512409!5m2!1sen!2sus",
        lat:"30.55528919999999" ,
        lng:"-97.7314224",
        //img_link: " " ,
        //books:  ,
        //authors ,
        id: 1

    },
    {
        libname: "Abernathy Public Library",
        region: "West Texas",
        city: "Abernathy",
        ser_pop: 3700,
        col_size: 9651,
        ann_circ: 4481,

        //instance page
        county: "Hale",
        address: "811 Avenue D .. ",
        phone: "806-298-2546",
        adname: "Jessica Stone",
        liburl: "https://abernathy.ploud.net/",

        //Other details
        desc: "Abarnathy Public Library is a public library, affiliated with ..",
        orgStruct: "This is a publicly funded and managed library",
        mapSrc: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3314.2427224669377!2d-101.84579544871754!3d33.831850580571555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86fe1b9515b0f61f%3A0x52625cdbd1ce38db!2sAbernathy%20Public%20Library!5e0!3m2!1sen!2sus!4v1634517512409!5m2!1sen!2sus",
        lat:"30.55528919999999" ,
        lng:"-97.7314224",
        //img_link: " " ,
        //books:  ,
        //authors ,
        id: 2
    },
    {
        libname: "Abernathy Public Library",
        region: "West Texas",
        city: "Abernathy",
        ser_pop: 3700,
        col_size: 9651,
        ann_circ: 4481,

        //instance page
        county: "Hale",
        address: "811 Avenue D .. ",
        phone: "806-298-2546",
        adname: "Jessica Stone",
        liburl: "https://abernathy.ploud.net/",

        //Other details
        desc: "Abarnathy Public Library is a public library, affiliated with ..",
        orgStruct: "This is a publicly funded and managed library",
        mapSrc: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3314.2427224669377!2d-101.84579544871754!3d33.831850580571555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86fe1b9515b0f61f%3A0x52625cdbd1ce38db!2sAbernathy%20Public%20Library!5e0!3m2!1sen!2sus!4v1634517512409!5m2!1sen!2sus",
        lat:"30.55528919999999" ,
        lng:"-97.7314224",
        //img_link: " " ,
        //books:  ,
        //authors ,
        id: 3
    }
]

export default libData;