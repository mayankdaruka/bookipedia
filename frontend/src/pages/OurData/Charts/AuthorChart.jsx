import React, { useState, useEffect } from "react";
import axios from "axios";
import {  useHistory} from "react-router-dom";
import { Row, Spinner } from "react-bootstrap";


import {
    ScatterChart,
    Scatter,
    XAxis,
    YAxis,
    ZAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Cell,
    ResponsiveContainer,
  } from 'recharts';
  
//   import { scaleOrdinal } from 'd3-scale';
// import { schemeCategory10 } from 'd3-scale-chromatic'; 
//   const colors = scaleOrdinal(schemeCategory10).range();
const COLORS = [
    "#EBA299",
    "#EBD399",
    "#EBEA99",
    // "#A9D1F7",
    // "#CC99FF",
  ];

  

// Add loading indicators if time 
const AuthorChart = () => {
  const [authData, setAuths] = useState([]); 
  const [loading, setLoading] = useState(true);
  const [authorMappings, setAuthorMappings] = useState([]);
  const history = useHistory();
  const url = 'https://api.bookipedia.me/api/all_authors'
  useEffect(() => {
    axios.get(url).then(res =>{
        setAuths(res.data.authors);
        getData(res.data.authors);
      }).then(() => setLoading(false))
  }, []);

  function getData(authData){
 var authorMappings = [];
 for (let i = 0; i < authData.length; i++) {
   if(authData[i]['average_book_ratings'] != 0 && authData[i]['num_works'] != null){
    authorMappings.push({
        author_id: authData[i]['author_id'],
        name: authData[i]['name'],
        average_book_ratings: Math.round(authData[i]['average_book_ratings'] * 100) / 100,
        num_works: authData[i]['num_works'],
    });
   }  
}

setAuthorMappings(authorMappings);
  }



//const regionMapping = Object.keys(dict).map((key) => {return{region: key, libs: dict[key]}});
console.log(authorMappings);





    return (
      <div>
     <h2 style={{ justifyContent: "center", display: "flex" }}>Average Author ratings vs Number of Books Published</h2>
       { loading ? (
      <Row style={{ justifyContent: "center", paddingTop: 30 }}>
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Row>
      ) : (
        <div  >
       <ScatterChart
          width={1000}
          height={700}
          data={authorMappings}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="num_works" name="Number of Published Books" unit=" books"  label={{
              value: "Number of Published Books",
              position: "insideBottom",
              offset: -5,
            }}
            tickCount={4}
            domain={[2.5, 5.5]}
            padding={{ left: 30, right: 30 }} />
          <YAxis type="number" dataKey="average_book_ratings" name="Average Book Rating" domain={[2.5, 5.5]} label={{
              value: "Average Book Rating",
              angle: -90,
              offset: -10,
            }} />
          <ZAxis type="category" dataKey="name" name="Author"  />
          {/* <Scatter name="A school" data={data} fill="#8884d8">
            {data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
            ))}
          </Scatter> */}
          <Scatter data={authorMappings} fill="#8884d8"  style={{ cursor: "pointer" }}  
          onClick={(data) => {history.push(`/authors/${data.author_id}`)} }>
              {authorMappings.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            ))}
            </Scatter>
         
          <Tooltip labelFormatter={() => { return ''; }} cursor={{ strokeDasharray: '3 3' }} />
          
        </ScatterChart> 
     
    </div>
    )}
    </div>
    )
  
  };
  
  export default AuthorChart;