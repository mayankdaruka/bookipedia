import React, { useState, useEffect } from "react";
import axios from "axios";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Cell, LabelList } from "recharts";
import { Row, Spinner } from "react-bootstrap";
import MyPagination from "../../../components/MyPagination"

const COLORS = [
  "#9fb1fc",
  "#EBD399",
  // "#EBEA99",
  // "#B4F0A7",
  // "#A9D1F7",
  // "#CC99FF",
];

// Add loading indicators if time 
const BookChart = () => {
  const [bookData, setBooks] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [pubMapping, setPubMapping] = useState([]);

  const url = 'https://api.bookipedia.me/api/all_books'
  useEffect(() => {
    axios.get(url).then(res => {
      setBooks(res.data.books);
      getData(res.data.books)
    }).then(() => setLoading(false))
  }, []);


  function getData(bookData) {
    console.log("This is bookData", bookData)
    console.log("This isloading", loading)
    var dict = {};

    for (let i = 0; i < bookData.length; i++) {
      if (bookData[i]['publisher'] != null) {
        let publisher = bookData[i]['publisher'];
        if (dict.hasOwnProperty(publisher)) {
          // dict[publisher] =  dict[publisher] +1;
          dict[publisher] += 1;
        } else {
          dict[publisher] = 1;
        }
      }
    }


    const pubMapping = Object.keys(dict).map((key) => { return { publisher: key, books: dict[key] } });
    pubMapping.sort((a, b) =>
      a["books"] > b["books"] ? -1 : 1
    )
    setPubMapping(pubMapping);
  }




  console.log(pubMapping);

  // pubMapping
  // const currentBooks = pubMapping.slice(0, 75);

  const booksPerPage = 16
  const indexOfLastItem = currentPage * booksPerPage;
  const indexOfFirstItem = indexOfLastItem - booksPerPage;
  const currentBooks = pubMapping.slice(indexOfFirstItem, indexOfLastItem);
  const total_pages = pubMapping.length % booksPerPage === 0 ? Math.floor(pubMapping.length / booksPerPage) : Math.floor(pubMapping.length / booksPerPage) + 1;
  console.log(currentBooks);
  // Change page, when this updates,the next time it renders the above will get a diff slice
  const paginate = (pageNumber) => setCurrentPage(pageNumber);




  return (
    <div>
        <h2 style={{ justifyContent: "center", display: "flex" }}>Number of Books per Publisher</h2>
       { loading ? (
      <Row style={{ justifyContent: "center", paddingTop: 30 }}>
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Row>
      ) : (
      <div  >



        <BarChart
          width={1000}
          height={700}
          data={currentBooks}
        // layout="vertical"
        >
          <CartesianGrid strokeDasharray="3 3" />

          {/* dataKey="name" textAnchor= "end" sclaeToFit="true" verticalAnchor= "start"  interval={0} angle= "-40" stroke="#8884d8"  */}
          <XAxis
            type="category"
            dataKey="publisher"
            // height= "10"
            label={{
              value: "Publisher",
              position: "insideBottom",
              offset: -5,
            }}
          />
          <YAxis
            type="number"
            dataKey="books"
            // domain={[0, 175]}
            label={{
              value: " Number of Books",
              position: 'insideLeft',
              angle: -90
            }}
          />

          <Tooltip />
          <Bar dataKey="books" fill="#8884d8" >
            <LabelList dataKey="Publisher" />
            {currentBooks.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            ))}
          </Bar >
        </BarChart>

        <MyPagination
          currentPage={currentPage}
          total_pages={total_pages}
          first={() => paginate(1)}
          prev={() => paginate(currentPage - 1 < 1 ? 1 : currentPage - 1)}
          next={() => paginate(currentPage + 1 > total_pages ? total_pages : currentPage + 1)}
          last={() => paginate(total_pages)} />


      </div>
      )}
    </div>

  )

};

export default BookChart;