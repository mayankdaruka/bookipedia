import React, { useState, useEffect, memo } from "react";
import axios from "axios";
import { PieChart, Pie, Tooltip, Cell, RadialBarChart, RadialBar, Sector } from "recharts";
import { Row, Spinner } from "react-bootstrap";


const COLORS = [
  "#EBA299",
  "#EBD399",
  "#EBEA99",
  "#B4F0A7",
  "#A9D1F7",
  "#CC99FF",
];


const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const {
          cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
          fill, payload, percent, value
        } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.region}</text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`Number of Libraries: ${value}`}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};


// Add loading indicators if time 
const LibChart = memo(() => {
  const [libData, setLibs] = useState([]);
  const [loading, setLoading] = useState(true);
  const [regionMapping, setRegionMapping] = useState([]);

  const url = 'https://api.bookipedia.me/api/all_libraries'

  useEffect(() => {
    axios.get(url).then(res =>{
      setLibs(res.data.libraries);
      getData(res.data.libraries);
    }).then(() => setLoading(false))
  }, []);


  function getData(libData){

  
  var dict = {};

  for (let i = 0; i < libData.length; i++) {
    if (libData[i]['region'] != null && libData[i]['region'].length < 20) {
      let region = libData[i]['region'];
      if (dict.hasOwnProperty(region)) {
        dict[region] = dict[region] + 1;
      } else {
        dict[region] = 1;
      }
    }
  }


  const regionMapping = Object.keys(dict).map((key) => { return { region: key, libs: dict[key] } });
  setRegionMapping(regionMapping);
  }
  const [count, setCount] = useState(0);

  function handleChange(){
    return (_, index) => {
      setCount(index) 
    }
}
//style={{ marginLeft: "200px" }} 
  return (
    <div>
      <h2 style={{ justifyContent: "center", display: "flex" }}>Number of Libraries in the different regions of Texas</h2>
      { loading ? (
     <Row style={{ justifyContent: "center", paddingTop: 30 }}>
       <Spinner animation="border" role="status">
         <span className="visually-hidden">Loading...</span>
       </Spinner>
     </Row>
     ) : (
    <div  >
      <PieChart width={1000} height={700} >
        <Pie
          activeIndex= {count}
          activeShape={renderActiveShape}
          innerRadius={250}
          outerRadius={300}
          data={regionMapping}
  
          labelLine={false}
          // outerRadius={200}
          fill="#8884d8"
          dataKey="libs"
          nameKey="region"
          onMouseEnter={handleChange()}
        >
          {regionMapping.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>

      </PieChart>
    </div>
      )}
      </div>

  )

});

export default LibChart ;

