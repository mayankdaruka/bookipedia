import React from "react";
import LibChart from "./Charts/LibChart";
import AuthorChart from "./Charts/AuthorChart";
import BookChart from "./Charts/BookChart";

const OurData = () => {



  return (
    <div>
        {/* <h style={{ justifyContent: "center", display: "flex", marginButton: "40px" }}>Our Data Visualization</h> */}
        {/* <h2 style={{ justifyContent: "center", display: "flex" }}>Number of Libraries in the different regions of Texas</h2> */}
        
        <BookChart />
        <br />
        <AuthorChart />
        <br />
        <LibChart />
        <br />
        
    </div>
  )

};

export default OurData;

