import React from "react";
import { CardGroup, Row, Col, Container } from "react-bootstrap";
import { BooksModel } from "../../assets";
import { House } from "../../assets";
import { Authors, Bk2 } from "../../assets";
import styles from "./home.module.css";
import { Link } from "react-router-dom";
import { HomeCard } from "./HomeCard"

import { SearchBar } from "../../components/SearchBar";

const homeCards = [
  {
    title: "Books",
    text: "Find some exciting books to add to your reading list!!",
    image: BooksModel,
    href: "/books",
    buttonText: "Search for Books"
  },
  {
    title: "Authors",
    text: "Lookup your favorite authors and check out their latest books",
    image: Authors,
    href: "/authors",
    buttonText: "Search for Authors"
  },
  {
    title: "Libraries",
    text: "Look for books you want at your local libraries",
    image: House,
    href: "/libs",
    buttonText: "Search for Libraries"
  }
]
const Home = () => (
  <div style={{
    backgroundImage: `url(${Bk2})`,
    backGroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100vw',
    height: '118vh',
  }}>

    {/* <Container style={{ backgroundColor: 'rgba(255, 255, 255, 0.3)', width: "100%", height: "200px" }}> */}
      <h1 className={styles.header}>
        <text style = {{marginTop : "20px"}}> Bookipedia </text>
      </h1>
      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",
          marginBottom: "80px",
        }}>
        <text> Hello! Are you looking to find some new books to read, learn more about the best selling authors, explore
        libraries in your area and find out which books they have available or perhaps all three ? Well you have come to the right site!
        </text>
        <text>
          </text>

      </p>

      <p
        style={{
          justifyContent: "center",
          display: "flex",
          marginLeft: "200px",
          marginRight: "200px",

        }}>

        <SearchBar model={"all"} />
      </p>
    {/* </Container> */}

    <CardGroup as={Row}  style={{
      justifyContent: "center",
      display: "flex",
      marginLeft: "80px",
      marginTop: "80px"
    }}>
      {
        homeCards.map(card =>
        (
          <Col>
            <Link to={card.href} style={{ textDecoration: 'none', color: 'inherit' }} >
              <HomeCard
                title={card.title}
                text={card.text}
                buttonText={card.buttonText}
                img={card.image}
                href={card.href} />
            </Link>
          </Col>
        ))
      }
    </CardGroup>
  </div >
);

export default Home;
