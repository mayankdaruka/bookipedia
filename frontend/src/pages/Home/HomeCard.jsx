// import React from "react";
// import { Card, Button, CardGroup } from 'react-bootstrap';
// import { BooksModel } from "../../../assets";
// import { pubHouse } from "../../../assets";
// import { Authors } from "../../../assets";
// width changes the card width, which is related to the photo
// Height of photo changes the image height

import {  Card } from "react-bootstrap"
import styles from "../../components/components.module.css";

export const HomeCard = ({title, text,  buttonText, img, href }) => 
(
    <Card style={{width: "20rem"}} className={styles.tool_card}>
    <Card.Img src={img}  style={{ height: "16rem"}} fluid/>
    <Card.Body>
      <Card.Title>{title}</Card.Title>
      <Card.Text>
          {text}
      </Card.Text>
    </Card.Body>
  </Card>
)

