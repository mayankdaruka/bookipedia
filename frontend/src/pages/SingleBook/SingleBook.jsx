import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { Button, Row, Col, Container, Spinner } from "react-bootstrap";
import { Styleshare } from "@styled-icons/simple-icons";
import styles from "./singleBook.module.css";
import axios from "axios";
import { Bk3, Bk5 } from "../../assets";

import { enumerate, get_proper_name } from "../../helper/functions";

import { LibResults } from "../Results/LibResults";
import { Grid } from "@material-ui/core";

import { LibList } from "../../components/LibList";

const SingleBook = () => {
  const { bookId } = useParams();
  const [thisBook, setBook] = useState([]);
  const [libData, setLibs] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get(`https://api.bookipedia.me/api/book/${bookId}`).then((res) => {
      setBook(res.data.book);
      setLibs(res.data.related_libraries);
      setLoading(false);
    });
  }, [bookId]);

  // const indexOfLastItemBooks = bookCurrentPage * booksPerPage;
  // const indexOfFirstItemBooks = indexOfLastItemBooks - booksPerPage;
  // const currentBooks = bookData.slice(indexOfFirstItemBooks, indexOfLastItemBooks);
  // const bookTotalPage = bookData.length%booksPerPage ===0? Math.floor(bookData.length/booksPerPage): Math.floor(bookData.length/booksPerPage) + 1;
  // const books_paginate = (pageNumber) => setBookCurrentPage(pageNumber);

  return (
    // <div style={{ backgroundImage: `url(${Bk5})`, backgroundSize: 'cover',  height: '150vh'}} >
    <div>
      {loading ? (
        <Row style={{ justifyContent: "center", paddingTop: 30 }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </Row>
      ) : (
        <Container>
          <div>
            {/* style={{
            backgroundImage: `url(${Bk3})`,
            backGroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            width: '100vw',
            height: '108vh',
            marginBottom : '20px' */}
            {/* TO DO: Style this, and diplay the images Authors: {enumerate(authors)} <br />in thisBook.id somewhere */}
            <h1 className={styles.header}>{thisBook.title}</h1>
            <p className={styles.author}>
              ── {("  ", enumerate(thisBook.authors))}
            </p>
            <p className={styles.picture}>
              <img
                src={thisBook.regular_thumbnail}
                style={{ marginRight: "80px" }}
              />
              Description:{thisBook.description} <br />
              {/* Subtitle: {thisBook.subtitle} <br /> */}
              Rating: {thisBook.average_rating} from {thisBook.num_ratings}{" "}
              ratings
              <br />
              Language : {get_proper_name(thisBook.language)} <br />
              ISBN: {bookId} <br />
              Publisher: {thisBook.publisher} <br />
              <br />
            </p>

            <container>
              <Col>
                <p style={{ marginLeft: "288px" }}>
                  <text className={styles.fontStype}>
                    Author Information: {enumerate(thisBook.authors)}
                    <br />
                  </text>
                  <Link to={`/authors/${thisBook.author_id}`}>
                    <Button
                      style={{ border: "none", backgroundColor: "purple" }}
                    >
                      View Authors Details
                    </Button>
                  </Link>
                </p>
              </Col>
            </container>
          </div>

          <Row
            style={{
              marginTop: "80px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Col>
              <p className={styles.video}>
                <iframe
                  style={{ width: 560, height: 315 }}
                  src={"https://www.youtube.com/embed/" + thisBook.video_url}
                  title="YouTube video player"
                  frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                ></iframe>
              </p>
            </Col>
            <Col>
              <h1 className={styles.fontStype} style={{}}>
                Library Results
              </h1>
              <LibList libs={libData} />
            </Col>
          </Row>

          {/* <img src={thisBook.image2} style={{ marginRight: "200px", height: "250px", marginLeft: "80px" }} /> */}
          {/* </p> */}
        </Container>
      )}
    </div>
    // </div>
  );
};

export default SingleBook;
