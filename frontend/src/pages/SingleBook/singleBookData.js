import hpImg from "../../assets//bookCovers/HarryPotter.jpg";
import alchemistImg from "../../assets//bookCovers/Alchemist.jpg";
import davinciImg from "../../assets//bookCovers/Davinci.jpg";
import hpAuthor from "../../assets//authorCovers/jkrowling.jpg";
import alchemistAuthor from "../../assets//authorCovers/paulo.jpg";
import davinciAuthor from "../../assets//authorCovers/danBrown.jpg";
import pubImg1 from "../../assets//pubHouse/bloomsburypublishing.jpg";
import pubImg3 from "../../assets//pubHouse/doubleday.png";
import pubImg2 from "../../assets//pubHouse/HarperCollins.jpeg";

const singleBookData = [
  {
    title: "Harry Potter and the Prisoner of Azkaban",
    //Model page
    authors: ["J. K. Rowling", "Paulo Coelho"],
    price: 11.5,
    currency: "$",
    page_count: 400,
    average_rating: 4.5,
    published_date: "1999",

    //Instance page
    publisher: "Bloomsbury",
    description: "Harry Potter and the Prisoner of Azkaban is a fantasy novel written by British author J. K. Rowling and is the third in the Harry Potter series. The book follows Harry Potter, a young wizard, in his third year at Hogwarts School of Witchcraft and Wizardry. Along with friends Ronald Weasley and Hermione Granger, Harry investigates Sirius Black, an escaped prisoner from Azkaban, the wizard prison, believed to be one of Lord Voldemort's old allies.",
    language: "English",
    isbn: "120445",
    subtitle: "and the Prisoner of Azkaban",
    num_ratings: 144,


    //media
    small_thumbnail: hpImg,
    regular_thumbnail: hpImg,
    vidsrc: "https://www.youtube.com/embed/2s_yXkcIQQk",
    buy_link: "https://play.google.com/store/books/details?id=DMLPz4rrBIIC&rdid=book-DMLPz4rrBIIC&rdot=1&source=gbs_api",

    //other instance pages
    author_id: 1,
    lib_id: 1,
    id: 1,
  },
  {
    title: "The Alchemist",
    //Model page
    authors: ["Paulo Coelho"],
    price: 11.5,
    currency: "$",
    page_count: 400,
    average_rating: 4.5,
    published_date: "1999",

    //Instance page
    publisher: "Bloomsbury",
    description: "Harry Potter and the Prisoner of Azkaban is a fantasy novel written by British author J. K. Rowling and is the third in the Harry Potter series. The book follows Harry Potter, a young wizard, in his third year at Hogwarts School of Witchcraft and Wizardry. Along with friends Ronald Weasley and Hermione Granger, Harry investigates Sirius Black, an escaped prisoner from Azkaban, the wizard prison, believed to be one of Lord Voldemort's old allies.",
    language: "Portuguese",
    isbn: "120445",
    subtitle: "and the Prisoner of Azkaban",
    num_ratings: 144,

    //media
    small_thumbnail: alchemistImg,
    regular_thumbnail: alchemistImg,
    vidsrc: "https://www.youtube.com/embed/c8v_EW_B68s",
    buy_link: "https://play.google.com/store/books/details?id=DMLPz4rrBIIC&rdid=book-DMLPz4rrBIIC&rdot=1&source=gbs_api",


    author_id: 2,
    lib_id: 2,
    id: 2,
  },
  {
    title: "The Da Vinci Code",
    //Model page
    authors: ["Dan Brown"],
    price: 11.5,
    currency: "$",
    page_count: 400,
    average_rating: 4.5,
    published_date: "1999",

    //Instance page
    publisher: "Bloomsbury",
    description: "Harry Potter and the Prisoner of Azkaban is a fantasy novel written by British author J. K. Rowling and is the third in the Harry Potter series. The book follows Harry Potter, a young wizard, in his third year at Hogwarts School of Witchcraft and Wizardry. Along with friends Ronald Weasley and Hermione Granger, Harry investigates Sirius Black, an escaped prisoner from Azkaban, the wizard prison, believed to be one of Lord Voldemort's old allies.",
    language: "English",
    isbn: "120445",
    subtitle: "and the Prisoner of Azkaban",
    num_ratings: 144,

    //media
    small_thumbnail: davinciImg,
    regular_thumbnail: davinciImg,
    vidsrc: "https://www.youtube.com/embed/Nh3E3mNtB5Q",
    buy_link: "https://play.google.com/store/books/details?id=DMLPz4rrBIIC&rdid=book-DMLPz4rrBIIC&rdot=1&source=gbs_api",

    author_id: 3,
    lib_id: 3,
    id: 3,
  },
];

export default singleBookData;
