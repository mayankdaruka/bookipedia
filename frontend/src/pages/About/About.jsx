import React, { useEffect, useState } from "react";
import { members, usernameEmailMap } from "./TeamInfo";
import { tools, ourOwnStuff, APIs } from "./ToolsInfo";
import { PROJECT_ID } from "./AboutConstants";
import { Spinner, Row, Col, Container, Card } from "react-bootstrap";
import GitlabStatsCard from "../../components/GitlabStatsCard";
import ToolsCard from "../../components/ToolsCard";
import styled from "styled-components";

const retrieveGitlabStats = async () => {
  let totalCommits = 0;
  let totalTests = 0;
  let totalIssues = 0;

  Object.keys(members).forEach((member_email) => {
    totalTests += members[member_email].tests;
  });

  const commitsPromise = await fetch(
    `https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/contributors`
  );
  const userCommits = await commitsPromise.json();

  const memberCommits = {};

  userCommits.forEach((user) => {
    if (members[user.email]) {
      memberCommits[user.email] =
        (memberCommits?.[user.email] ?? 0) + user.commits;
    }
    totalCommits += user.commits;
  });

  Object.keys(memberCommits).forEach((member) => {
    members[member].commits = memberCommits[member];
  });

  const issuePaginationLength = 100;
  let page = 1;
  let issueList = [];
  let issuePage = [];

  do {
    issuePage = await fetch(
      `https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues?per_page=${issuePaginationLength}&page=${page++}`
    );
    issuePage = await issuePage.json();
    issueList = [...issueList, ...issuePage];
  } while (issuePage.length === 100);

  totalIssues = issueList.length;

  const memberIssues = {};

  issueList.forEach((issue) => {
    const userEmail = usernameEmailMap[issue.author.username];
    if (userEmail)
      memberIssues[userEmail] = (memberIssues?.[userEmail] ?? 0) + 1;
    // if (userEmail) members[userEmail].issues += 1;
  });

  Object.keys(memberIssues).forEach((member) => {
    members[member].issues = memberIssues[member];
  });

  return [totalCommits, totalIssues, totalTests];
};

function About() {
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);
  const [loaded, setLoaded] = useState(false);

  useEffect(async () => {
    const totalStats = await retrieveGitlabStats();
    setTotalCommits(totalStats[0]);
    setTotalIssues(totalStats[1]);
    setTotalTests(totalStats[2]);
    setLoaded(true);
  }, []);

  const teamMembers = Object.values(members);

  return (
    <div>
      <div>
        <div
          style={{
            textAlign: "center",
            fontWeight: 500,
            fontSize: 30,
            margin: 10,
          }}
        >
          About Us
        </div>
        <div
          style={{
            textAlign: "center",
            fontSize: 20,
            fontWeight: 300,
            marginRight: 80,
            marginLeft: 80,
          }}
        >
          Bookipedia is a platform where avid book readers can learn more
          about authors and books they're interested in, as well as come across
          new books from different genres. We make it easy for them to find books
          of similar taste based on the author at their local libraries, and provide
          interesting facts about each author to make the experience more enjoyable and educational.
        </div>
        <div
          style={{
            textAlign: "center",
            fontWeight: 500,
            fontSize: 30,
            margin: 10,
          }}
        >
          Our Team
        </div>
        {loaded ? (
          <Container>
            <Row style={{ justifyContent: "center" }} xs={2} md={3}>
              {Object.values(members).map((member) => (
                <Col>
                  <GitlabStatsCard member={member} />
                </Col>
              ))}
            </Row>
          </Container>
        ) : (
          <Row style={{ justifyContent: "center" }}>
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </Row>
        )}
        {loaded && (
          <div>
            <div
              style={{
                textAlign: "center",
                fontWeight: 500,
                fontSize: 30,
                margin: 10,
              }}
            >
              Total Stats
            </div>
            <div
              style={{
                textAlign: "center",
                fontSize: 20,
                fontWeight: 300,
                marginRight: 80,
                marginLeft: 80,
              }}
            >
              <Card style={{ margin: 10, marginTop: 20 }}>
                <Card.Text>Total Commits: {totalCommits}</Card.Text>
              </Card>
              <Card style={{ margin: 10 }}>
                <Card.Text>Total Issues: {totalIssues}</Card.Text>
              </Card>
              <Card style={{ margin: 10, marginBottom: 20 }}>
                <Card.Text>Total Tests: {totalTests}</Card.Text>
              </Card>
            </div>
          </div>
        )}
        <div
          style={{
            textAlign: "center",
            fontWeight: 500,
            fontSize: 30,
            margin: 10,
          }}
        >
          Tools
        </div>
        <Container style={{ marginBottom: 100 }}>
          <Row xs={2} md={3}>
            {tools.map((tool) => (
              <Col style={{ justifyContent: "center" }}>
                <ToolsCard tool={tool} />
              </Col>
            ))}
          </Row>
        </Container>
      </div>
      <div
        style={{
          textAlign: "center",
          fontWeight: 500,
          fontSize: 30,
          margin: 10,
        }}
      >
        APIs
      </div>
      <div
        style={{
          textAlign: "center",
          fontSize: 20,
          fontWeight: 300,
          marginRight: 80,
          marginLeft: 80,
        }}
      >
        <Container style={{ marginBottom: 100 }}>
          <Row xs={2} md={3}>
            {APIs.map((api) => (
              <Col style={{ justifyContent: "center" }}>
                <ToolsCard tool={api} />
              </Col>
            ))}
          </Row>
        </Container>
      </div>
      <div
        style={{
          textAlign: "center",
          fontWeight: 500,
          fontSize: 30,
          margin: 10,
        }}
      >
        Our Postman API and GitLab Repository
      </div>

      <div style={{ display: "flex", flexDirection: "row" }}></div>
      {ourOwnStuff.map((tool) => (
        <div style={{ flex: 1 }}>
          <ToolsCard tool={tool} />
        </div>
      ))}
    </div>
  );
}

export default About;
