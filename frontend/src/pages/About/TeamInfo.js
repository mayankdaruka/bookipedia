import * as Constants from "./AboutConstants";
import MayankImage from "../../assets/TeamImages/MayankImage.png";
import WilliamImage from "../../assets/TeamImages/WilliamImage.png";
import MeganImage from "../../assets/TeamImages/MeganImage.jpg";
import JohnImage from "../../assets/TeamImages/JohnImage.png";
import JinImage from "../../assets/TeamImages/JinImage.png";

export const members = {
  [Constants.JIN_EMAIL]: {
    name: "Jin Huang",
    bio: "I'm a fourth year Computer Science at UT Austin. I grew up in China and moved to United States about four years ago. When I am free, I enjoy dancing, playing teniss and reading novals",
    image: JinImage,
    role: "Frontend",
    commits: 0,
    issues: 0,
    tests: 10,
  },
  [Constants.JOHN_EMAIL]: {
    name: "John Wang",
    bio: "I‘m a fourth year CS major at UT Austin. I grew up in a small city in China, and move to United States after I graduate from high school. I love basketball and spend my free time to watch basketball and play basketball video games.",
    image: JohnImage,
    role: "Backend",
    commits: 0,
    issues: 0,
    tests: 0,
  },
  [Constants.WILLIAM_EMAIL]: {
    name: "William Shi",
    bio: "Hi! I'm a junior studying CS and Plan II. I don't know exactly what I want to do in the future but I'm interested in SWE, data science, and game design. I like to play video games, watch anime, read webnovels, or play the piano.",
    image: WilliamImage,
    role: "Frontend/Backend",
    commits: 0,
    issues: 0,
    tests: 0,
  },
  [Constants.MEGAN_EMAIL]: {
    name: "Megan Noronha",
    bio: "I'm a fourth year Computer Science and Astronomy double major at UT Austin, I grew up in Abu Dhabi, UAE. In my free time, I enjoy watching Naruto, working out, and sleeping.",
    image: MeganImage,
    role: "Frontend",
    commits: 0,
    issues: 0,
    tests: 10,
  },
  [Constants.MAYANK_EMAIL]: {
    name: "Mayank Daruka",
    bio: "I am a Junior at UT Austin studying Computer Science and Math. I am from Houston, TX and in my free time I enjoy playing tennis, watching cricket, exercising, and listening to music!",
    image: MayankImage,
    role: "Backend",
    commits: 0,
    issues: 0,
    tests: 30,
  },
};

export const usernameEmailMap = {
  [Constants.JIN_USERNAME]: Constants.JIN_EMAIL,
  [Constants.JOHN_USERNAME]: Constants.JOHN_EMAIL,
  [Constants.MAYANK_USERNAME]: Constants.MAYANK_EMAIL,
  [Constants.MEGAN_USERNAME]: Constants.MEGAN_EMAIL,
  [Constants.WILLIAM_USERNAME]: Constants.WILLIAM_EMAIL,
};
