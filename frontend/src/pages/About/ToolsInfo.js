import AWSLogo from "../../assets/ToolsImages/AWSLogo.png";
import GitlabLogo from "../../assets/ToolsImages/GitlabLogo.png";
import PostmanLogo from "../../assets/ToolsImages/PostmanLogo.png";
import ReactBootstrapLogo from "../../assets/ToolsImages/ReactBootstrapLogo.png";
import ReactLogo from "../../assets/ToolsImages/ReactLogo.png";
import NamecheapLogo from "../../assets/ToolsImages/NamecheapLogo.png";
import NYTimesLogo from "../../assets/ToolsImages/NYTimesLogo.png";
import GoogleBooksLogo from "../../assets/ToolsImages/GoogleBooksLogo.png";
import PRHLogo from "../../assets/ToolsImages/PenguinRandomHouseLogo.png";
import YouTubeLogo from "../../assets/ToolsImages/YouTubeLogo.png";
import DockerLogo from "../../assets/ToolsImages/DockerLogo.png";
import PostgresLogo from "../../assets/ToolsImages/PostgresLogo.png";
import FlaskLogo from "../../assets/ToolsImages/FlaskLogo.png";

export const tools = [
  {
    name: "React",
    image: ReactLogo,
    link: "https://reactjs.org/docs/getting-started.html",
    description: "Front-end framework for our web application and UI",
  },
  {
    name: "React Bootstrap",
    image: ReactBootstrapLogo,
    link: "https://react-bootstrap.github.io/getting-started/introduction/",
    description:
      "UI library that we used with React for nicely designed components",
  },
  {
    name: "Gitlab",
    image: GitlabLogo,
    link: "https://gitlab.com/",
    description: "Version control software where our code is located",
  },
  {
    name: "Postman",
    image: PostmanLogo,
    link: "https://www.postman.com/",
    description: "Used to test our APIs and write our API documentation",
  },
  {
    name: "AWS",
    image: AWSLogo,
    link: "https://aws.amazon.com/",
    description:
      "Used to connect our web application to our domain and host our API",
  },
  {
    name: "Namecheap",
    image: NamecheapLogo,
    link: "https://www.namecheap.com/",
    description: "Used to host our web application at a certain domain",
  },
  {
    name: "Flask",
    image: FlaskLogo,
    link: "https://flask.palletsprojects.com/en/2.0.x/",
    description: "Used to create our API for books, authors and libraries",
  },
  {
    name: "PostgreSQL",
    image: PostgresLogo,
    link: "https://www.postgresql.org/",
    description:
      "The database we used to store all our book, author and library records that our API could retrieve",
  },
  {
    name: "Docker",
    image: DockerLogo,
    link: "https://www.docker.com/",
    description:
      "Used to create a Docker image for both our backend and frontend",
  },
];

export const APIs = [
  {
    name: "Google Books API",
    image: GoogleBooksLogo,
    link: "https://developers.google.com/books",
    description:
      "Used to retrieve information about books based on ISBN number and author name - came in the form of an external API with various endpoints and documentation",
  },
  {
    name: "Penguin Random House API",
    image: PRHLogo,
    link: "https://developer.penguinrandomhouse.com/io-docs",
    description:
      "Used to retrieve information about authors of best selling books - came in the form of an external API with various endpoints and documentation",
  },
  {
    name: "YouTube API",
    image: YouTubeLogo,
    link: "https://developers.google.com/youtube/v3",
    description:
      "Used to retrieve videos about books and authors for our users - came in the form of an external API with various endpoints and documentation",
  },
];

export const ourOwnStuff = [
  {
    name: "Gitlab Repository",
    image: GitlabLogo,
    link: "https://gitlab.com/mayankdaruka/bookipedia",
    description: "A link to our code repository",
  },
  {
    name: "Postman Documentation",
    image: PostmanLogo,
    link: "https://documenter.getpostman.com/view/17742251/UV5c9vFb",
    description: "A link to our API documentation made using Postman",
  },
];
