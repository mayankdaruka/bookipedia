export const JOHN_EMAIL = "zhongyi.wang@utexas.edu";
export const MEGAN_EMAIL = "megan.noronha@utexas.edu";
export const MAYANK_EMAIL = "mayankdaruka@utexas.edu";
export const JIN_EMAIL = "huangjin@cs.utexas.edu";
export const WILLIAM_EMAIL = "williamlongshi@gmail.com";

export const JOHN_USERNAME = "zhongyi.wang";
export const MEGAN_USERNAME = "megan.noronha";
export const MAYANK_USERNAME = "mayankdaruka";
export const JIN_USERNAME = "huangjin88";
export const WILLIAM_USERNAME = "shiguy";

export const PROJECT_ID = "29903038";
