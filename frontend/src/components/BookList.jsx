import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";
import { useTheme } from  '@material-ui/core/styles';




export const BookList = ({books}) =>
  (
    <TableContainer style={{maxHeight: 300}} >
      <Table sx={{ minWidth: 650 }} aria-label="simple table" stickyHeader>
        <TableHead>
          <TableRow>
          <TableCell>Book Title </TableCell>
           <TableCell align="right">Price</TableCell>
             <TableCell align="right">Rating&nbsp;</TableCell>
            <TableCell align="right">Page Count&nbsp;</TableCell>
            <TableCell align="right">Published Year&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {books.map((book) => (
            <TableRow
            component={Link}to={`/books/${book.book_id}`} style={{ textDecoration: 'none', color: 'black' }}
              key={book.libname}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
       
                    <TableCell component="th" scope="row">
                 {book.title}
               </TableCell>
              <TableCell align="right">{book.price}</TableCell>
              <TableCell align="right">{book.average_rating}</TableCell>
              <TableCell align="right">{book.page_count}</TableCell>
               <TableCell align="right">{book.published_year}</TableCell>
              
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
