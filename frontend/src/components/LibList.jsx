
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";
import { useTheme } from  '@material-ui/core/styles';



export const LibList = ({libs}) =>
  (
    <TableContainer style={{maxHeight: 300}} >
      <Table sx={{ minWidth: 650 }} aria-label="simple table" stickyHeader>
        <TableHead>
          <TableRow>
            <TableCell>Library Name </TableCell>
            <TableCell align="right">County&nbsp;</TableCell>
            <TableCell align="right">Region&nbsp;</TableCell>
            <TableCell align="right">Phone Number&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {libs.map((lib) => (
            <TableRow
            component={Link} to={`/libs/${lib.id}`} style={{ textDecoration: 'none', color: 'black' }}
              key={lib.libname}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
       
              <TableCell component="th" scope="row">
                {lib.libname}
              </TableCell>
              <TableCell align="right">{lib.county}</TableCell>
              <TableCell align="right">{lib.region}</TableCell>
              <TableCell align="right">{lib.phone}</TableCell>
              
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
