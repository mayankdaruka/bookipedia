import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";
import { useTheme } from  '@material-ui/core/styles';


// "auth_id": 3313,
// "author_id": 3313,
// "average_book_ratings": 3.875,
// "average_page_counts": 260,
// "description": "<p><b>KEVIN BROCKMEIER</b>&#160;is the author of the memoir&#160;<i>A Few Seconds of Radiant Filmstrip;</i>&#160;the novels&#160;<i>The Illumination, The Brief History of the Dead,</i>&#160;and&#160;<i>The Truth About Celia;</i>&#160;the story collections&#160;<i>The Ghost Variations, Things That Fall from the Sky&#160;</i>and&#160;<i>The View from the Seventh Layer;</i>&#160;and the children&rsquo;s novels&#160;<i>City of Names and Grooves: A Kind of Mystery</i>. His work has been translated into seventeen languages. He teaches frequently at the Iowa Writers&rsquo; Workshop and lives in Little Rock, Arkansas, where he was raised.</p>",
// "first_name": "Kevin",
// "image": "https://images.randomhouse.com/author/3313",
// "last_name": "Brockmeier",
// "name": "Kevin Brockmeier",
// "num_works": 4,
// "video_url": " i2SAphfesv",
// "works": [
//     "9780307377104",
//     "9780307379580",
//     "9780307387769",
//     "9780375727696"
// ]

export const AuthorList = ({authors}) =>
  (
    <TableContainer style={{maxHeight: 300}} >
      <Table sx={{ minWidth: 650 }} aria-label="simple table" stickyHeader>
        <TableHead>
          <TableRow>
          <TableCell>Author Name </TableCell>
           <TableCell align="right">Number of works</TableCell>
             <TableCell align="right">Average Books Ratins&nbsp;</TableCell>
            <TableCell align="right"> Average Books Length&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {authors.map((author) => (
            <TableRow
            component={Link}to={`/authors/${author.auth_id}`} style={{ textDecoration: 'none', color: 'black' }}
              key={author.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
       
                    <TableCell component="th" scope="row">
                 {author.name}
               </TableCell>
              <TableCell align="right">{author.num_works}</TableCell>
              <TableCell align="right"> {Math.round(author.average_book_ratings* 100) / 100} </TableCell>
              <TableCell align="right">{author.average_page_counts}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
