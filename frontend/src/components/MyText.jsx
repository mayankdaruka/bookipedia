const helperText = (as, children, color = "dark") => {
    switch (as) {
        case "h1":
            return <h1 className={`text${color}`}>{children}</h1>
        case "h2":
            return <h2 className={`text${color}`}>{children}</h2>
        case "h3":
            return <h3 className={`text${color}`}>{children}</h3>
        case "h4":
            return <h4 className={`text${color}`}>{children}</h4>
        case "h5":
            return <h5 className={`text${color}`}>{children}</h5>
        case "h6":
            return <h6 className={`text${color}`}>{children}</h6>
       default:
            return <p className={`text${color}`}>{children}</p>
    }
}

const MyText = ({ children, as, color }) => (<>{helperText(as, children, color)}</>)


export default MyText