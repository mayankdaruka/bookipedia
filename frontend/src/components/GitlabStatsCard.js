// import React from "react";
import { Card, Container, Row, Col } from "react-bootstrap";
import css_styles from "./components.module.css";
import styled from "styled-components";
// import { FiGitCommit } from "react-icons/fi";

import {
  GitCommit,
  GitBranch,
  GitPullRequest,
} from "@styled-icons/boxicons-regular";

import { CheckmarkCircleOutline } from "@styled-icons/evaicons-outline";

const styles = {
  cardImage: {
    borderRadius: "50%",
    height: "13rem",
    width: "13rem",
    objectFit: "cover",
    alignSelf: "center",
    margin: "15px",
  },
};

const CardRow = styled(Row)`
  margin: 10px;
`;

function GitlabStatsCard({ member }) {
  const { name, bio, image, role, commits, issues, tests } = member;

  return (
    <Card className={css_styles.gitlab_card}>
      <Card.Img src={image} style={styles.cardImage} />
      <Card.Title style={{ marginBottom: "15px" }}>{name}</Card.Title>

      <Container style={{ marginBottom: "15px" }}>
        <Row>
          <Col>
            <CardRow className="justify-content-md-center"> Commits </CardRow>
            <CardRow className="justify-content-md-center">
              <GitCommit size="30" />
            </CardRow>
            <CardRow className="justify-content-md-center">{commits}</CardRow>
          </Col>
          <Col>
            <CardRow className="justify-content-md-center"> Tests </CardRow>
            <CardRow>
              <CheckmarkCircleOutline size="30" />
            </CardRow>
            <CardRow className="justify-content-md-center"> {tests} </CardRow>
          </Col>
          <Col>
            <CardRow className="justify-content-md-center"> Issues </CardRow>
            <CardRow>
              <GitBranch size="30" />
            </CardRow>
            <CardRow className="justify-content-md-center"> {issues} </CardRow>
          </Col>
        </Row>
      </Container>
      <Card.Text style={{ marginBottom: 15, marginRight: 10, marginLeft: 10 }}>
        {bio}
      </Card.Text>
    </Card>
  );
}

export default GitlabStatsCard;
