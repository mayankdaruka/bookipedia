import React from "react";
import { Card } from "react-bootstrap";
import css_styles from "./components.module.css";
// import styled from "styled-components";

function ToolsCard({ tool }) {
  const { name, image, link, description } = tool;

  return (
    <Card className={css_styles.tool_card}>
      <a href={link}>
        <Card.Img
          src={image}
          style={{
            objectFit: "contain",
            height: "10rem",
            marginTop: "20px",
          }}
        />
      </a>
      <Card.Text
        style={{
          marginTop: 15,
          marginBottom: 15,
          fontWeight: 700,
          fontSize: 20,
          textAlign: "center",
        }}
      >
        {name}
      </Card.Text>
      <Card.Text
        style={{
          margin: 10,
          fontWeight: 200,
          fontSize: 16,
          textAlign: "center",
        }}
      >
        {description}
      </Card.Text>
    </Card>
  );
}

export default ToolsCard;
