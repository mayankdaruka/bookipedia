import React, { useState} from "react";
// import styles from "./book.module.css";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";

import {
    filterOptionsMap,
    filterTitlesMap,
} from "../helper/filterValues"


import {updateParams} from "../helper/functions"

export const DropdownFilter = ({ name, hook, hook2 }) => {
    const [params, setParams] = hook
    const [currentPage, setCurrentPage] = hook2
    const title = filterTitlesMap[name]
    // console.log(title)

    const options = filterOptionsMap[title]

    const p = new URLSearchParams(window.location.search)
    let state = ''
    if(p.get(name)){
        state = p.get(name)
    }
    const [selected, setSelected] = useState(state); //change to title

    function handleChange(name, setParams, params){
        return (event) => {
            console.log("value", event)
            // console.log("help", filter)
            if(event.target.value == "None"){
                setSelected('') // takes in the value
                updateParams(name, "", setParams, params)
                setCurrentPage(1)
            }else{
                setSelected(event.target.value) // takes in the value
           updateParams(name, event.target.value, setParams, params)
           setCurrentPage(1)
           

            }
           
        }
    }
    


    return (
        <FormControl variant="outlined" fullWidth size="small">
            <InputLabel htmlFor="agent-simple">{title}</InputLabel>
            <Select 
 
                onChange={handleChange(name, setParams, params)}
                // value={value}
                value={selected}
            >

                {Object.keys(options).map((key) => {
                             return <MenuItem value={key}>{options[key]}</MenuItem>;
                        })}
            </Select>
        </FormControl>

    );
}
