import {  Col, Form, InputGroup, Button } from "react-bootstrap";
import React, { useRef } from "react";

export const SearchBar = ({ hook, model }) => {
    //const [params, setParams] = hook
    const searchText = useRef();

    function routeChange() {
        return () => {
            window.location.assign(`/search/q=${searchText.current.value}/model=${model}`);
        }
    }

    const keyPress = (event) => {
        if (event.key === 'Enter') {
            return window.location.assign(`/search/q=${searchText.current.value}/model=${model}`);
        }
    };

    return (
        <Form.Row >
            <Form.Group as={Col} style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
            }}>
                <InputGroup style={{ width: "1000px", display : "flex" }}  >

                    <Form.Control
                        type="text"
                        placeholder="Search here.."
                        ref={searchText}
                        onKeyPress={keyPress}
                    />
                </InputGroup>
                <InputGroup.Append>
                    <Button
                        id =  "user-button-click"
                        onClick={routeChange()}
                        style={{
                            width: "80px",
                            marginLeft: "5px",
                            height: "38px",
                            backgroundColor: "purple",
                            border: "none"
                        }} size="sm">
                        Search
                    </Button>
                </InputGroup.Append>
            </Form.Group>
        </Form.Row>

    )
};

