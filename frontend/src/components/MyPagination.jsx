import React from 'react';
import {Pagination } from "react-bootstrap"
import styles from "./components.module.css";
//   display: flex;
  // justify-content: center;
  // margin-bottom: 30px;
  // margin-top: 45px;

const MyPagination = ({ 
  currentPage,
  total_pages,
  first = () =>{} , 
  last = () =>{}, 
  next = () =>{}, 
  prev = () =>{}
}) => 
(    <>
  <p   style={{ justifyContent: "center", display: "flex", marginBottom:"10px", marginTop:"40px"}}  >
  {/* Total instances: {bookData.length} /  {bookData.length} <br /> */}
  Total pages: {total_pages}
</p>
    <Pagination className={styles.pagination}  >
    <Pagination.First onClick={first} />
    <Pagination.Prev onClick={prev} />

    <Pagination.Item active> {currentPage < 1 ? 1 : currentPage}</Pagination.Item>

    <Pagination.Next onClick={next} />

    <Pagination.Last onClick={last} />
  </Pagination>
  </>
);


export default MyPagination;