import { render, screen, fireEvent, getByTestId } from '@testing-library/react';
import {DropdownFilter} from "./components/DropdownFilter"
import App from './App';
import { BrowserRouter as Router } from "react-router-dom";
import Enzyme, { mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";


Enzyme.configure({ adapter: new Adapter() });

import {
  Navigation,
  Home,
  About,
  Books,
  Authors,
  Houses,
  Posts,
  Libsa
} from "./components";
import {enumerate} from "./helper/functions"
import {Fragment} from "react"
//import {fireEvent, getByTestId} from "react-testing-library";

test('Test 1: The Library', () => {
  render(<App />);
  const linkElement = screen.findAllByText(/Libraries/i)
  expect(linkElement).toBeTruthy();
});

test('Test 2: The Book model', () => {
  render(<App />);
  const linkElement = screen.findAllByText(/Books/i)
  expect(linkElement).toBeTruthy();
 });

test('Test 3: The Author model', () => {
  render(<App />);
  const linkElement = screen.findAllByText(/Authors/i)
  expect(linkElement).toBeTruthy();
 });

 test("Test 4: Books per page variable existing", () => {
  const { container } = render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/booksPerPage/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 5: Authors per page variable existing", () => {
  const { container } = render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/authsPerPage/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 6: Libs per page variable existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/libsPerPage/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 7: Paginate variable existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/paginate/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 8: BookData variable existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/bookData/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 9: AuthData variable existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/authData/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 10: LibData variable existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/libData/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 11: Searchbar existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/SearchBar/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});


test("Test 12: DropdownBox existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/DropdownFilter/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});


test("Test 13: FilterOptionsMap existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/filterOptionsMap/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});


test("Test 14: FilterTitlesMap existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/filterTitlesMap/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});


test("Test 15: Sorting params existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/param_names/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});


test("Test 16: Paginate existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/paginate/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});


test("Test 17: ModelName existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/modelName/i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 18: Construct Authors URLParams  existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/constructAuthorsURLParams /i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});

test("Test 19: Construct Book URLParams  existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/constructBookURLParams /i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});


test("Test 20: Construct Libs URLParams  existing", () => {
  render(<App />);
  //booksPerPage
  const linkElement = screen.findAllByText(/constructLibsURLParams /i)
  //const countValue = getByTestId(container, "countvalue");
  expect(linkElement).toBeTruthy();
});



test("Test 21: Checks number of Search Bar in home page", () => {
  const wrapper = shallow(<Home />);
  expect(wrapper.find("SearchBar").length).toEqual(1);
});











// test('Authors model link', () => { authsPerPage
//   render(<Home />);
//   const linkElement = screen.findAllByText(/Authors/i)
//   expect(linkElement).toBeTruthy();
// });

// const add = (a, b) => a + b
// test('Library model link', () => {
//   const result = 'a b c '
//   expect(enumerate(['a', 'b', 'c'])).toBe([<>a </>, <>b </>, <>c </>]
//     )

// });


