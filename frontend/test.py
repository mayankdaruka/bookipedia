import os
from sys import platform

if __name__ == "__main__":
    # Use chromedriver based on OS
    if platform == "win32":
        PATH = "./frontend/chromedriver.exe"
    elif platform == "linux":
        PATH = "./frontend/chromedriver_linux"
    else:
        # print("Unsupported OS")
        PATH = "./frontend/chromedriver_mac"
        exit(-1)

    # Run all of the gui tests
    os.system("python3 ./frontend/guitests.py " + PATH)
